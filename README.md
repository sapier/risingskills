# Rising Skills Plugin

**Description**:  
Rising skills is supposed to be an implementation of various skills within the game.
Goal is to learn by using a skill and gradually get benefits when using a skill.
Benefits supposed to be implemented are e.g. speeding up the skill, improving the yield, reducing tool deterioration, reducing stamina consumption.
Another aspect might be enabling the use of skills to different resources, e.g. in order to cut down a certain tree you might need to be at a certain skill level.

By pressing the "J" key you will be able to see your current job progression.  
*Note:* it'd be nice if this could be added to the same mechanism as inventory and crafting yet making it nice is next step

Generic crafting delay:
If you want to add a generic crafting delay to all crafts remove the file **disable_crafting_delay** from the plugin folder.
Be aware that this feature doesn't work very well by now.


## License
LGPLv2.1

## Dependencies
None

## Jobs
The initial skills are selected to discover how far this can be done using the current plugin api. They should cover most mechanisms required for jobs but are by no means complete.  
Skills intended to be implemented first are:
### Logging
Features:
  - higher skill level increases damage done (slightly)
  - insufficient skill level (compared to tree) reduces damage (dramatically)
    - reduces damage to standing trees
    - reduces damage fallen trees 
       ==> not yet possible
  - insufficient skill level (compared to tool) 
    - reduces damage to standing trees
    - reduces damage to fallen trees
       ==> not yet possible
    - increases deterioration (dramatically)
       You shouldn't wield tools you don't know how to use them
  - higher level increases yield (slightly)
    - Idea is to increase the number of pieces dropped by a fallen tree (by one or two at most)
        ==> not yet possible
  - higher skill reduces deterioration of tool (slightly)
  
    
### Mining
Features:
  - [TBD] higher skill level increases yield
     - increase amount of collected stones (slightly)
     - increase amount of mined rocks
     - increases damage (slightly)
  - [TBD] higher skill level gives chance for high tier ore
     - provide random chance for rare ore when mining terrain (very small)
     - provide random chance for rare ore
  - insufficient skill level (tool to rock)
      - increases wear (dramaticaly)
  - insufficient skill level (player to rock)
      - reduces damage (dramaticaly)
      - increases wear (slightly)
  - provide small amount of xp for collecting stones
  - provide xp for mining rocks
  - provide xp for mining terrain
  - Hud to make mining more easy.
     lvl 3 -> horizontal angle
     lvl 4 -> direction
     lvl 6 -> elevation

### Herbalism
Features:
  BLOCKED as plant definitions can't be modified and no kind of plants provide anything right now
  - higher skill level increases yield
     - increase amount of collected plants
  - herb collection requires specific skill level
      - don't allow gathering below the level
  - some herbs require specific tools
  - insufficient skill level (tool to rock)
      - increases wear (dramaticaly)
  - insufficient skill level (player to rock)
      - reduces damage (dramaticaly)
      - increases wear (slightly)

### Carpentry
  - introduce crafting time for carpentry recipes
  - require skill level for higher tier crafts
  - TBD: increase yield for higher skill levels
  - provide xp for crafting
  - TBD:reduce crafting time

### Smelting
  - Notify player about smelting complete if close by furnace
  - skill increase range of smelting complete message
  - TBD: provide chance for extra items with higher skill level
  - TBD: chance for smelting failure

### Smithing
TBD


## Known Issues
 - no event triggered when hitting fallen tree
 - no way to dynamicaly modify yield for a plant reciep
 - no (known) way to configure "job" key
 - no (known) way to integrate to inventory/crafting ui
 - [HAVE WORKAROUND] "tier" of resources not yet in official recipes
 - [HAVE WORKAROUND] missing event before player elements are destroyed
 - it's not clear if the integer provided as TerrainID is identical to the enum Plants.TerrainMaterialType
 - damage can't be modified in PlayerHitTerrainEvent
 - Inventory is lacking a addItem(Items.ItemDefinition item, int amount) function
 - [HAVE UGLY WORKAROUND] the furnace generates no events at all