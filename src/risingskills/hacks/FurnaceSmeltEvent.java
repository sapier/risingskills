/*
 *
 */
package risingskills.hacks;

import net.risingworld.api.events.Event;

// TODO: Auto-generated Javadoc
/**
 * The Class FurnaceSmeltEvent.
 */
public class FurnaceSmeltEvent extends Event {

	/**
	 * Instantiates a new furnace smelt event.
	 *
	 * @param metaobjectid the metaobjectid
	 * @param playername the playername
	 * @param srcitemname the srcitemname
	 * @param smelteditemid the smelteditemid
	 */
	FurnaceSmeltEvent(Long metaobjectid, String playername, String srcitemname, Long smelteditemid)
	{
		furnaceID = metaobjectid;
		playerName = playername;
		srcItemName = srcitemname;
		itemID = smelteditemid;
	}

	/**
	 * Gets the player name.
	 *
	 * @return the player name
	 */
	public String getPlayerPutItemName()
	{
		return playerName;
	}

	/**
	 * Gets the furnace meta object ID.
	 *
	 * @return the furnace meta object ID
	 */
	public Long getFurnaceMetaObjectID()
	{
		return furnaceID;
	}

	/**
	 * Gets the smelted item ID.
	 *
	 * @return the smelted item ID
	 */
	public Long getSmeltedItemID()
	{
		return itemID;
	}

	/**
	 * Gets the src item name.
	 *
	 * @return the src item name
	 */
	public String getSrcItemName()
	{
		return srcItemName;
	}

	/** The player name. */
	private String playerName;

	/** The furnace ID. */
	private Long furnaceID;

	/** The src item name. */
	private String srcItemName;

	/** The item ID. */
	private Long itemID;
}
