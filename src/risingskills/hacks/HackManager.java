/*
 *
 */
package risingskills.hacks;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import net.risingworld.api.Plugin;
import net.risingworld.api.events.Event;
import net.risingworld.api.events.Listener;
import risingskills.hacks.LogfileParser.LogFileParserEvent;

// TODO: Auto-generated Javadoc
/**
 * The Class hackManager.
 */
public class HackManager {

	/**
	 * The Enum HackEnum.
	 */
	public enum HackEnum
	{
		/** The rising skills hack logfileparser. */
		RISING_SKILLS_HACK_LOGFILEPARSER,

		/** The rising skills hack shadow furnace. */
		RISING_SKILLS_HACK_SHADOW_FURNACE
	}

	/**
	 * Instantiates a new hack manager.
	 *
	 * @param manager the manager
	 */
	public HackManager(Plugin manager)
	{
		knownHacks = new HashMap<HackEnum, Object>();
		registredListeners = new ArrayList<Listener>();

		String logfilepath = manager.getPath() + "/../../Logs/Player.log";
		LogfileParser parser = new LogfileParser(this, logfilepath);
		knownHacks.put(HackEnum.RISING_SKILLS_HACK_LOGFILEPARSER, parser);

		ShadowFurnaceManager furnaceManager =  new ShadowFurnaceManager(this, manager);
		knownHacks.put(HackEnum.RISING_SKILLS_HACK_SHADOW_FURNACE, furnaceManager);
	}

	/**
	 * Register listener.
	 *
	 * @param listener the listener
	 */
	public void registerEventListener(Listener listener)
	{
		registredListeners.add(listener);
	}

	/**
	 * Process logfile parser event.
	 *
	 * @param event the event
	 */
	public void processLogfileParserEvent(LogFileParserEvent event)
	{

		ShadowFurnaceManager sfm = (ShadowFurnaceManager) knownHacks.get(HackEnum.RISING_SKILLS_HACK_SHADOW_FURNACE);
		sfm.processLogfileEvent(event);
	}

	/**
	 * Raise event.
	 *
	 * @param event the event
	 */
	public void raiseEvent(Event event)
	{
		if (event == null)
		{
			System.out.println("HackManager: raising event with null, yeah, of course...");
			return;
		}

		for (Listener target : registredListeners)
		{
			Method methods[] = target.getClass().getMethods();

			for (Method m : methods)
			{
				int numparams = m.getParameterCount();

				if (numparams != 1) {
					continue;
				}

				boolean isOnEventMethod = false;

				Annotation annos[] = m.getAnnotations();
				for (Annotation a : annos)
				{
					if (a.annotationType().toString().equals("interface net.risingworld.api.events.EventMethod"))
					{
						isOnEventMethod = true;
						break;
					}
				}

				if (isOnEventMethod)
				{
					Class<?>[] parameterstypes = m.getParameterTypes();

					if (event.getClass() == parameterstypes[0])
					{
						System.out.println("Method: " + m.getName() + " is an event handler and fits to current event " + event.getClass().getName());
						try {
							m.invoke(target, event);
						} catch (IllegalAccessException | InvocationTargetException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	/** The registred listeners. */
	private ArrayList<Listener> registredListeners;

	/** The known hacks. */
	private HashMap<HackEnum, Object> knownHacks;
}
