/*
 *
 */
package risingskills.hacks;

import net.risingworld.api.events.Event;

// TODO: Auto-generated Javadoc
/**
 * The Class FurnaceIngotTakenEvent.
 */
public class FurnaceIngotTakenEvent extends Event {

	/**
	 * Instantiates a new furnace smelt event.
	 *
	 * @param metaobjectid the metaobjectid
	 * @param playerputname the playerputname
	 * @param playertakenname the playertakenname
	 * @param srcitemname the srcitemname
	 * @param smelteditemid the smelteditemid
	 */
	FurnaceIngotTakenEvent(Long metaobjectid, String playerputname, String playertakenname, String srcitemname, Long smelteditemid)
	{
		furnaceID = metaobjectid;
		playerPutName = playerputname;
		playerTakeName = playertakenname;
		srcItemName = srcitemname;
		itemID = smelteditemid;
	}

	/**
	 * Gets the player name who did place the item.
	 *
	 * @return the player name
	 */
	public String getPlayerPutItemName()
	{
		return playerPutName;
	}

	/**
	 * Gets the player name who did take the item.
	 *
	 * @return the player name
	 */
	public String getPlayerTakeItemName()
	{
		return playerTakeName;
	}

	/**
	 * Gets the furnace meta object ID.
	 *
	 * @return the furnace meta object ID
	 */
	public Long getFurnaceMetaObjectID()
	{
		return furnaceID;
	}

	/**
	 * Gets the smelted item ID.
	 *
	 * @return the smelted item ID
	 */
	public Long getSmeltedItemID()
	{
		return itemID;
	}

	/**
	 * Gets the src item name.
	 *
	 * @return the src item name
	 */
	public String getSrcItemName()
	{
		return srcItemName;
	}

	/** The player name. */
	private String playerPutName;

	/** The player take name. */
	private String playerTakeName;

	/** The furnace ID. */
	private Long furnaceID;

	/** The src item name. */
	private String srcItemName;

	/** The item ID. */
	private Long itemID;
}
