/*
 *
 */
package risingskills.hacks;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;

import net.risingworld.api.Plugin;
import net.risingworld.api.World;
import net.risingworld.api.definitions.Definitions;
import net.risingworld.api.definitions.Items;
import net.risingworld.api.definitions.Items.ItemDefinition;
import net.risingworld.api.definitions.Objects;
import net.risingworld.api.events.EventMethod;
import net.risingworld.api.events.Listener;
import net.risingworld.api.events.player.PlayerPickupItemEvent;
import net.risingworld.api.objects.MetaObject;
import net.risingworld.api.objects.Player;
import net.risingworld.api.objects.WorldItem;
import net.risingworld.api.objects.world.ObjectElement;
import net.risingworld.api.utils.Utils;
import net.risingworld.api.utils.Vector3f;
import risingskills.hacks.LogfileParser.LogFileParserEvent;
import risingskills.hacks.LogfileParser.LogFileParserFiretouchEvent;
import risingskills.hacks.LogfileParser.LogFileParserItemRemovedEvent;
import risingskills.hacks.LogfileParser.LogFileParserPlaceEvent;
import risingskills.hacks.LogfileParser.LogFileParserTransformEvent;

// TODO: Auto-generated Javadoc
/**
 * The Class ShadowFurnaceManager.
 */
public class ShadowFurnaceManager implements Listener {

	/**
	 * The Class ShadowFurnace.
	 */
	private class ShadowFurnace
	{

		/**
		 * Instantiates a new shadow furnace.
		 *
		 * @param object the object
		 */
		public ShadowFurnace(MetaObject object)
		{
			metaObjectID = object.getID();
			worldpos = object.getWorldPosition();
			itemsLinked = new ArrayList<FurnaceItem>();
		}

		/** The meta object ID. */
		public Long metaObjectID;

		/** The worldpos. */
		public Vector3f worldpos;

		/**
		 * Gets the new furnace item.
		 *
		 * @return the new furnace item
		 */
		public FurnaceItem getNewFurnaceItem()
		{
			return new FurnaceItem();
		}

		/**
		 * Put item.
		 *
		 * @param item the item
		 */
		public void putItem(FurnaceItem item)
		{
			// TODO add some checks e.g. max capacity
			itemsLinked.add(item);
			//System.out.println(plugin.getName() + " ShadowFurnaceManager: DEBUG furnace: " + metaObjectID + " now contains " + itemsLinked.size() + " items");
		}

		/**
		 * Take item.
		 *
		 * @param rItem the r item
		 * @return true, if successful
		 */
		public boolean takeItem(FurnaceItem rItem)
		{
			ArrayList<FurnaceItem> newlist = new ArrayList<FurnaceItem>();

			boolean found = false;

			for ( FurnaceItem item: itemsLinked)
			{
				boolean addtolist = true;

				if ( (item.putItemName.equals(rItem.putItemName)) &&
						(item.putPlayerName.equals(rItem.putPlayerName)) )
				{
					float distance = Utils.MathUtils.distance(item.putLocation.x, item.putLocation.y, item.putLocation.z,
							rItem.putLocation.x, rItem.putLocation.y, rItem.putLocation.z);

					//System.out.println(plugin.getName() + " ShadowFurnaceManager: DEBUG takeItem distance=" + distance);
					// for now consider a distance of < 0.1 as same item
					if (distance < 0.1)
					{
						found = true;
						continue;
					}
				}

				if (addtolist)
				{
					newlist.add(item);
				}
			}
			itemsLinked = newlist;
			//System.out.println(plugin.getName() + " ShadowFurnaceManager: DEBUG furnace: " + metaObjectID + " now contains " + itemsLinked.size() + " items");
			return found;
		}

		/**
		 * Item at.
		 *
		 * @param pos the pos
		 * @return true, if successful
		 */
		public FurnaceItem itemAt(Vector3f pos)
		{
			for ( FurnaceItem item: itemsLinked)
			{
				float distance = Utils.MathUtils.distance(item.putLocation.x, item.putLocation.y, item.putLocation.z,
						pos.x, pos.y, pos.z);

				if (distance < 0.1)
				{
					return item;
				}
			}
			return null;
		}

		/**
		 * Update item.
		 *
		 * @param uItem the u item
		 * @return true, if successful
		 */
		public boolean updateItem(FurnaceItem uItem)
		{
			if (takeItem(uItem))
			{
				putItem(uItem);
				return true;
			}

			return false;
		}

		/**
		 * The Class FurnaceItem.
		 */
		public class FurnaceItem
		{
			/** The put item name. */
			public String putItemName;

			/** The put player name. */
			public String putPlayerName;

			/** The put location. */
			public Vector3f putLocation;

			/** The put timestamp. */
			@SuppressWarnings("unused")
			public Long putTimestamp;

			/** The put item ID. */
			@SuppressWarnings("unused")
			public Long putItemID;

			/** The transform timestamp. */
			@SuppressWarnings("unused")
			public Long transformTimestamp;

			/** The transform name. */
			@SuppressWarnings("unused")
			public String transformName;

			/** The transform location. */
			@SuppressWarnings("unused")
			public Vector3f transformLocation;

			/** The transformed item ID. */
			public Long transformedItemID;
		}

		/** The items linked. */
		private ArrayList<FurnaceItem> itemsLinked;
	}

	/**
	 * The Class PickupItemAction.
	 */
	private class RecentlyPickedUpItem {

		/** The time stamp. */
		public long timeStamp;

		/** The player name. */
		public String playerName;

		/** The item def. */
		public Items.ItemDefinition itemDef;

		/** The position. */
		Vector3f position;
	}

	/**
	 * The Class TransformAction.
	 */
	private class TransformAction {

		/** The time stamp. */
		public long timeStamp;

		/** The origin name. */
		@SuppressWarnings("unused")
		public String originName;

		/** The transformed name. */
		public String transformedName;
	}

	/**
	 * Instantiates a new shadow furnace manager.
	 *
	 * @param manager the manager
	 * @param smanager the smanager
	 */
	public ShadowFurnaceManager(HackManager manager, Plugin smanager)
	{
		hackManager = manager;
		plugin = smanager;

		knownFurnaces = new HashMap<Long, ShadowFurnace>();
		recentlyPickedUpOres = new HashMap<Long, RecentlyPickedUpItem>();
		itemsTransformed = new ArrayList<TransformAction>();

		plugin.registerEventListener(this);
	}

	/**
	 * Process logfile event.
	 *
	 * @param event the event
	 */
	public void processLogfileEvent(LogFileParserEvent event)
	{
		long ts = Instant.now().getEpochSecond();

		// these are all events for smelting
		if (event instanceof LogFileParserTransformEvent)
		{
			processItemTransform((LogFileParserTransformEvent) event, ts);
		}
		else if (event instanceof LogFileParserPlaceEvent)
		{
			processItemPlaced((LogFileParserPlaceEvent) event, ts);
		}
		else if (event instanceof LogFileParserFiretouchEvent)
		{
			processItemTouchesFireEvent((LogFileParserFiretouchEvent) event, ts);
		}
		else if (event instanceof LogFileParserItemRemovedEvent)
		{
			processItemRemoved((LogFileParserItemRemovedEvent) event, ts);
		}
	}

	/**
	 * Process item placed.
	 *
	 * @param event the event
	 * @param tsNow the ts now
	 */
	private void processItemPlaced(LogFileParserPlaceEvent event, long tsNow)
	{

		// only ores can be placed to a furnace for now
		Items.ItemDefinition itemDef = Definitions.getItemDefinition(event.placedName);
		if (itemDef.type != Items.Type.Ore)
		{
			// no need to remember stuff which ain't ore (e.g. logs)
			//System.out.println(plugin.getName() + " ShadowFurnaceManager: DEBUG processItemPlaced: " + event.placedName + " not ore");
			return;
		}

		// collect information not present in event but required
		MetaObject furnace = World.getMetaObject(event.placedToMetaObjectID);

		// we couldn't identify the furnace for this event
		if (furnace == null)
		{
			System.out.println(plugin.getName() + " ShadowFurnaceManager: PROBLEM furnace couldn't be found");
			return;
		}

		ObjectElement furnace_obj = furnace.getRelatedObject();

		// object wan't placed to furnace so we're not responsible for it
		if (furnace_obj.getDefinition().type != Objects.Type.Furnace)
		{
			System.out.println(plugin.getName() + " ShadowFurnaceManager: PROBLEM furnace found but type was wrong");
			return;
		}

		ShadowFurnace shadowFurnace = null;
		if (!knownFurnaces.containsKey(event.placedToMetaObjectID))
		{
			shadowFurnace = new ShadowFurnace(furnace);
			knownFurnaces.put(event.placedToMetaObjectID, shadowFurnace);
		}
		else {
			shadowFurnace = knownFurnaces.get(event.placedToMetaObjectID);
		}

		if (furnace.getID() != shadowFurnace.metaObjectID)
		{
			System.out.println(plugin.getName() + " ShadowFurnaceManager: PROBLEM furnace an shadowFurnace metaobject ids don't match");
			return;
		}

		// todo check world positions are still fine

		ShadowFurnace.FurnaceItem toadd = shadowFurnace.getNewFurnaceItem();
		toadd.putItemName = event.placedName;

		// CHANCE FOR ERROR: let's assume the player closest to the furnace did place the object
		Player player = net.risingworld.api.Server.findNearestPlayer(furnace.getWorldPosition());
		toadd.putPlayerName = player.getName();

		// CHANCE FOR ERROR: try to find the new item by excluding the new ones
		WorldItem newitem = null;
		WorldItem[] allitems = World.getAllItemsInRange(shadowFurnace.worldpos, 2.0f);
		for ( WorldItem tocheck : allitems)
		{
			if (shadowFurnace.itemAt(tocheck.getPosition()) == null)
			{
				newitem = tocheck;
			}
		}
		if (newitem == null)
		{
			System.out.println(plugin.getName() + " ShadowFurnaceManager: PROBLEM didn't find a new item withn the range of the furnace");
			return;
		}

		toadd.putItemID = newitem.getGlobalID();
		toadd.putLocation = newitem.getPosition();
		shadowFurnace.putItem(toadd);
	}

	/**
	 * Provess item removed.
	 *
	 * @param event the event
	 * @param tsNow the ts now
	 */
	private void processItemRemoved(LogFileParserItemRemovedEvent event, long tsNow)
	{
		Player player = null;
		RecentlyPickedUpItem rItem = null;


		// check if this is a item we need to care about
		ItemDefinition srcItemDef = Definitions.getItemDefinition(event.itemName);
		if (srcItemDef.type != Items.Type.Ore)
		{
			//System.out.println(plugin.getName() + " ShadowFurnaceManager: DEBUG processItemRemoved ignored " + event.itemName);
			return;
		}

		// check if this item was really just picked up by the upper player
		if (recentlyPickedUpOres.containsKey(event.itemID))
		{
			rItem = recentlyPickedUpOres.get(event.itemID);
			player = net.risingworld.api.Server.getPlayerByName(rItem.playerName);
		}
		// doesn't look like this item was picked up lately so we can't do anything with this event
		else
		{

			System.out.println(plugin.getName() + " ShadowFurnaceManager: PROBLEM couldn't find a recently picked up matching item");
			return;
		}

		if (!rItem.itemDef.name.equals(event.itemName))
		{
			System.out.println(plugin.getName() + " ShadowFurnaceManager: PROBLEM recently picked up item name mismatch " + rItem.itemDef.name + "!=" + event.itemName);
			return;
		}

		// collect information not present in event but required
		MetaObject furnace = World.getMetaObject(event.removedFromMetaObjectID);

		// we couldn't identify the furnace for this event
		if (furnace == null)
		{
			System.out.println(plugin.getName() + " ShadowFurnaceManager: PROBLEM couldn't find the furnace the item was removed");
			return;
		}

		ObjectElement furnace_obj = furnace.getRelatedObject();

		// object wan't placed to furnace so we're not responsible for it
		if (furnace_obj.getDefinition().type != Objects.Type.Furnace)
		{
			System.out.println(plugin.getName() + " ShadowFurnaceManager: PROBLEM the furnace the item was removed wasn't actually a furnace");
			return;
		}

		//		// check if this player was really close to the furnace
		//		Player playerclose = net.risingworld.api.Server.findNearestPlayer(furnace.getWorldPosition());
		//
		//		// check if the players do match ... this might cause issues if more than one player is close to the furnace
		//		// but that's just how it is for now
		//		if (!playerclose.getName().equals(player.getName()))
		//		{
		//			System.out.println(plugin.getName() + " ShadowFurnaceManager: PROBLEM player picking up (" + player.getName() + ") and players close (" + playerclose.getName() + ") to furnace aren't same can't process this");
		//		}

		Items.ItemDefinition itemDef = rItem.itemDef;

		if ( itemDef.type != Items.Type.Ore )
		{
			System.out.println(plugin.getName() + " ShadowFurnaceManager: PROBLEM the itemid of an element in recently picked up ores didn't result in an ore iremdef");
			return;
		}

		ShadowFurnace shadowFurnace = knownFurnaces.get(event.removedFromMetaObjectID);

		if (shadowFurnace == null)
		{
			System.out.println(plugin.getName() + " ShadowFurnaceManager: PROBLEM there's no way we can remove an item from a shadowFurnace we don't even know about");
			return;
		}

		if (furnace.getID() != shadowFurnace.metaObjectID)
		{
			System.out.println(plugin.getName() + " ShadowFurnaceManager: PROBLEM furnace an shadowFurnace metaobject ids don't match");
			return;
		}

		ShadowFurnace.FurnaceItem toremove = shadowFurnace.getNewFurnaceItem();

		toremove.putItemName = event.itemName;
		toremove.putPlayerName = player.getName();
		toremove.putLocation = rItem.position;

		if ( shadowFurnace.takeItem(toremove) )
		{
			// cleanup entry
			recentlyPickedUpOres.remove(event.itemID);
		}
		else
		{
			System.out.println(plugin.getName() + " ShadowFurnaceManager: PROBLEM seems like shadowFurnace didn't have a matching item");
			return;
		}
	}

	/**
	 * Process item transform.
	 *
	 * @param event the event
	 * @param tsNow the ts now
	 */
	private void processItemTransform(LogFileParserTransformEvent event, Long tsNow)
	{
		TransformAction toadd = new TransformAction();
		toadd.originName = event.originalName;
		toadd.transformedName = event.transformedName;
		toadd.timeStamp = tsNow;

		itemsTransformed.add(toadd);

		//System.out.println(plugin.getName() + " ShadowFurnaceManager: DEBUG adding transform entry: " + toadd.originName + " => " + toadd.transformedName + " itemsTransformed.size()=" + itemsTransformed.size());
	}

	/**
	 * Process item touches fire event.
	 *
	 * @param event the event
	 * @param ts the ts
	 */
	private void processItemTouchesFireEvent(LogFileParserFiretouchEvent event, Long ts)
	{
		// get oldest transform event
		TransformAction action = removeItemTransformEvent(event.itemName);

		if (action == null)
		{
			// there's no transform stored, this is a regular case if no smelting was completed yet so just leave here
			System.out.println(plugin.getName() + " ShadowFurnaceManager: PROBLEM no transform event stored for " + event.itemName);
			return;
		}

		// try to find the item in the world
		WorldItem item = net.risingworld.api.World.getItem(event.itemID);
		if (item == null)
		{
			System.out.println(plugin.getName() + " ShadowFurnaceManager: PROBLEM sorry but we didn't find this item in the world id: " + event.itemID);
		}

		Items.ItemDefinition itemDef = item.getDefinition();
		// case smelting was complete
		if (itemDef.type != Items.Type.Ingot)
		{
			//System.out.println(plugin.getName() + " ShadowFurnaceManager: DEBUG touch fire event for non ingot item " + event.itemName);
			return;
		}
		// try to match the item to a furnace
		ArrayList<Long> furnaces_nearby = getFurnaceInRange(item.getPosition(), 2f);

		boolean handled = false;

		// look for the item in all closeby furnaces
		for ( Long furnaceID : furnaces_nearby)
		{
			ShadowFurnace shadowFurnace = knownFurnaces.get(furnaceID);

			ShadowFurnace.FurnaceItem fItem = shadowFurnace.itemAt(item.getPosition());
			// this furnace doesn't have a item at the given position
			if (fItem == null)
			{
				continue;
			}

			handled = true;

			fItem.transformedItemID = event.itemID;
			fItem.transformLocation = item.getPosition();
			fItem.transformName = event.itemName;
			fItem.transformTimestamp = ts;

			// generate event towards the skills
			FurnaceSmeltEvent eventelement = new FurnaceSmeltEvent(furnaceID, fItem.putItemName, fItem.putItemName, fItem.transformedItemID);
			hackManager.raiseEvent(eventelement);

			shadowFurnace.updateItem(fItem);
			break;
		}

		if (!handled) {
			System.out.println(plugin.getName() + " ShadowFurnaceManager: PROBLEM ItemID: " + event.itemID + " " + itemDef.type.toString() + " not found in any shadowFurnace");
		}
	}

	/**
	 * Removes the item transform event.
	 *
	 * @param name the name
	 * @return the transform action
	 */
	private TransformAction removeItemTransformEvent(String name)
	{
		//System.out.println(plugin.getName() + " ShadowFurnaceManager: DEBUG looking for transform entry: "  + name + " transforms known: " + itemsTransformed.size());

		Long oldestTS = 0l;

		// find the oldest matching timestamp
		for ( TransformAction action : itemsTransformed)
		{
			if (!action.transformedName.equals(name))
			{ continue; }

			if (oldestTS == 0)
			{
				oldestTS = action.timeStamp;
			}
			else if (action.timeStamp < oldestTS)
			{
				oldestTS = action.timeStamp;
			}
		}

		if (oldestTS != 0)
		{
			TransformAction retval = null;

			// remove exactly one element with matching timestamp
			boolean found = false;
			ArrayList<TransformAction> newlist = new ArrayList<TransformAction>();
			for ( TransformAction action : itemsTransformed)
			{
				if ((found == false) && (action.timeStamp == oldestTS))
				{
					found = true;
					retval = action;
					continue;
				}
				// remove items which are too old as the log line is expected immediately after the transform
				if (action.timeStamp + 60 < Instant.now().getEpochSecond())
				{
					continue;
				}
				newlist.add(action);
			}
			itemsTransformed = newlist;
			return retval;
		}

		return null;
	}

	/**
	 * Gets the furnace in range.
	 *
	 * @param pos the pos
	 * @param distance the distance
	 * @return the furnace in range
	 */
	private ArrayList<Long> getFurnaceInRange(Vector3f pos, float distance)
	{
		ArrayList<Long> retval = new ArrayList<Long>();
		for ( Long key : knownFurnaces.keySet())
		{
			Vector3f furnacepos = knownFurnaces.get(key).worldpos;
			float distance_calculated = Utils.MathUtils.distance(pos.x,  pos.y, pos.z, furnacepos.x, furnacepos.y, furnacepos.z);

			//System.out.println(plugin.getName() + " " + skillName + ": furnace distance=" + distance_calculated);

			if (distance_calculated < distance)
			{
				retval.add(key);
			}
		}

		return retval;
	}

	/** The hack manager. */
	private HackManager hackManager;

	/** The plugin. */
	private Plugin plugin;

	/** The known furnaces. */
	private HashMap<Long, ShadowFurnace> knownFurnaces;

	/** The recently placed ores. */
	private HashMap<Long, RecentlyPickedUpItem> recentlyPickedUpOres;

	/** The items transformed. */
	private ArrayList<TransformAction> itemsTransformed;

	/**
	 * On player pickup item event.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onPlayerPickupItemEvent(PlayerPickupItemEvent event)
	{
		long tsNow = Instant.now().getEpochSecond();

		// remove item log line is typically immediately after the pickup so let's just remove older events here
		for ( Long key : recentlyPickedUpOres.keySet())
		{
			// 10 seconds should be more than enough
			if (recentlyPickedUpOres.get(key).timeStamp + 10 < tsNow)
			{
				recentlyPickedUpOres.remove(key);
			}
		}

		RecentlyPickedUpItem toadd = new RecentlyPickedUpItem();
		toadd.timeStamp = tsNow;
		toadd.playerName = event.getPlayer().getName();
		toadd.itemDef = event.getItem().getDefinition();
		toadd.position = event.getItem().getPosition();

		if (toadd.itemDef.type == Items.Type.Ore)
		{
			recentlyPickedUpOres.put(event.getItem().getGlobalID(), toadd);
			return;
		}

		if (toadd.itemDef.type == Items.Type.Ingot)
		{
			WorldItem item = event.getItem();

			// try to match the item to a furnace
			ArrayList<Long> furnaces_nearby = getFurnaceInRange(item.getPosition(), 2f);

			for ( Long furnaceID : furnaces_nearby)
			{
				ShadowFurnace shadowFurnace = knownFurnaces.get(furnaceID);

				ShadowFurnace.FurnaceItem fItem = shadowFurnace.itemAt(item.getPosition());
				// this furnace doesn't have a item at the given position
				if (fItem == null)
				{
					continue;
				}

				FurnaceIngotTakenEvent eventelement = new FurnaceIngotTakenEvent(furnaceID, event.getPlayer().getName(), fItem.putPlayerName, fItem.putItemName, fItem.transformedItemID);
				hackManager.raiseEvent(eventelement);

				// remove item from shadow furnace
				shadowFurnace.takeItem(fItem);
				break;
			}
		}
	}
}
