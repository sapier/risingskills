/*
 *
 */
package risingskills.hacks;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.time.Instant;

import net.risingworld.api.Timer;

// TODO: Auto-generated Javadoc
/**
 * The Class LogfileParser.
 */
public class LogfileParser implements Runnable
{
	/**
	 * The Class LogFileParserEvent.
	 */
	public class LogFileParserEvent
	{

		/** The time stamp. */
		public long timeStamp;
	}

	/**
	 * The Class LogFileParserTransformEvent.
	 */
	public class LogFileParserTransformEvent extends LogFileParserEvent
	{

		/** The original name. */
		public String originalName;

		/** The new name. */
		public String transformedName;
	}

	/**
	 * The Class LogFileParserPlaceEvent.
	 */
	public class LogFileParserPlaceEvent extends LogFileParserEvent
	{

		/** The placed name. */
		public String placedName;

		/** The placed to name. */
		public String placedToName;

		/** The placed to meta object ID. */
		public Long placedToMetaObjectID;
	}

	/**
	 * The Class LogFileParserFiretouchEvent.
	 */
	public class LogFileParserFiretouchEvent extends LogFileParserEvent
	{

		/** The item name. */
		public String itemName;

		/** The item ID. */
		public Long itemID;
	}

	/**
	 * The Class LogFileParserItemRemovedEvent.
	 */
	public class LogFileParserItemRemovedEvent extends LogFileParserEvent
	{

		/** The item name. */
		public String itemName;

		/** The item ID. */
		public Long itemID;

		/** The taken from meta object ID. */
		public Long removedFromMetaObjectID;
	}

	/**
	 * Instantiates a new logfile parser.
	 *
	 * @param manager the manager
	 * @param filename the filename
	 */
	public LogfileParser(HackManager manager, String filename)
	{
		fileName = filename;
		hackManager = manager;

		readTimer = new Timer(1.0f,0,-1,this);
		readTimer.start();
	}

	/**
	 * Run.
	 */
	@Override
	public void run() {

		long ts = Instant.now().getEpochSecond();
		RandomAccessFile logfile;

		try {
			logfile = new RandomAccessFile(fileName, "r");
			logfile.seek(bytesRead);
			String data;
			while ((data = logfile.readLine()) != null)
			{
				// Transform item ironore -> ironingot
				if (data.startsWith("Transform item"))
				{
					String[] parts = data.split(" ", 10);
					LogFileParserTransformEvent event = new LogFileParserTransformEvent();
					event.transformedName = parts[4];
					event.originalName = parts[2];
					event.timeStamp = ts;
					hackManager.processLogfileParserEvent(event);
				}

				// CLIENT: Place item ironore (FurnaceSlot) in primitivefurnace (2154624)
				if (data.startsWith("CLIENT: Place item")) // not sure if this will be available on a server
				{
					String[] parts = data.split(" ", 10);
					LogFileParserPlaceEvent event = new LogFileParserPlaceEvent();
					event.placedName = parts[3];
					event.placedToName = parts[6];
					event.placedToMetaObjectID = Long.valueOf(parts[7].substring(1, parts[7].length()-1));
					event.timeStamp = ts;
					hackManager.processLogfileParserEvent(event);
				}

				// ITEM ironingot (3187377) TOUCHES FIRE!
				if (data.contains("TOUCHES FIRE!"))
				{
					String[] parts = data.split(" ", 10);
					LogFileParserFiretouchEvent event = new LogFileParserFiretouchEvent();
					event.itemName = parts[1];
					event.itemID = Long.valueOf(parts[2].substring(1,  parts[2].length()-1));
					event.timeStamp = ts;
					hackManager.processLogfileParserEvent(event);
				}

				// Remove item ironingot (2154625) from meta object 2154624
				if ((data.startsWith("Remove item")) && (data.contains("from meta object")))
				{
					String[] parts = data.split(" ", 10);
					LogFileParserItemRemovedEvent event = new LogFileParserItemRemovedEvent();
					event.itemName = parts[2];
					event.removedFromMetaObjectID = Long.valueOf(parts[7]);
					event.itemID = Long.valueOf(parts[3].substring(1, parts[3].length()-1));
					event.timeStamp = ts;
					hackManager.processLogfileParserEvent(event);
				}

				bytesRead += data.length();
			}

			logfile.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/** The read timer. */
	private Timer readTimer;

	/** The file name. */
	private String fileName;

	/** The bytes read. */
	private int bytesRead;

	/** The skill manager. */
	private HackManager hackManager;

}
