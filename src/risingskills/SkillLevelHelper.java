/*
 *
 */
package risingskills;

import java.util.HashMap;

import risingskills.SkillManager.SkillEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class SkillLevelHelper.
 */
public class SkillLevelHelper
{

	/** The base XP amount. */
	private static HashMap<SkillEnum, Integer> baseXPAmount;

	/**
	 * Gets the base XP amount.
	 *
	 * @param skill the skill
	 * @return the base XP amount
	 */
	static public int getBaseXPAmount(SkillEnum skill)
	{
		if (baseXPAmount.containsKey(skill))
		{
			return baseXPAmount.get(skill);
		}

		return 50;
	}

	/**
	 * Gets the base XP amount.
	 *
	 * @param skill the skill
	 * @param value the amount to set
	 */
	static public void setBaseXPAmount(SkillEnum skill, Integer value)
	{
		if (baseXPAmount == null)
		{
			baseXPAmount = new HashMap<SkillEnum, Integer>();
		}

		baseXPAmount.put(skill,  value);
	}

	/**
	 * Gets the skill level.
	 *
	 * @param skill the skill
	 * @param experience the experience
	 * @return the skill level
	 */
	static public int getSkillLevel(SkillEnum skill, int experience)
	{
		int level = 0;
		int XPRequired = getXPForLevel(skill, level);
		while (experience >= XPRequired)
		{
			//System.out.println("XP left: " + String.valueOf(experience));
			experience -= XPRequired;
			level ++;
			XPRequired = getXPForLevel(skill, level);
		}
		return level;
	}

	/**
	 * Gets the XP in level.
	 *
	 * @param skill the skill
	 * @param experience the experience
	 * @return the XP in level
	 */
	static public int getXPInLevel(SkillEnum skill, int experience)
	{
		int level = 0;
		int XPRequired = getXPForLevel(skill, level);
		while (experience > XPRequired)
		{
			//System.out.println("XP left: " + String.valueOf(experience));
			experience -= XPRequired;
			level ++;
			XPRequired = getXPForLevel(skill, level);
		}
		return experience;
	}

	/**
	 * Gets the XP for level.
	 *
	 * @param skill the skill
	 * @param level the level
	 * @return the XP for level
	 */
	static public int getXPForLevel(SkillEnum skill, int level)
	{
		int xpforlevel = (int) Math.round(Math.scalb(Double.valueOf(getBaseXPAmount(skill)),level));
		//System.out.println("Level " + String.valueOf(level) + " requires " + String.valueOf(xpforlevel) + " exp");
		return xpforlevel;
	}
}
