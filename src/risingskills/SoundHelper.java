/*
 *
 */
package risingskills;

import java.util.HashMap;

import net.risingworld.api.Plugin;
import net.risingworld.api.assets.SoundAsset;
import net.risingworld.api.objects.Player;

// TODO: Auto-generated Javadoc
/**
 * The Class SoundHelper.
 */
public class SoundHelper {

	/**
	 * Instantiates a new sound helper.
	 *
	 * @param owner the owner
	 */
	public SoundHelper(Plugin owner)
	{
		knownSounds = new HashMap<String, SoundAsset>();
		knownSounds.put("SND_LEVEL_UP", SoundAsset.loadFromFile(owner.getPath() + "/sounds/level_up.mp3"));
	}

	/**
	 * Play sound.
	 *
	 * @param player the player
	 * @param soundtoplay the soundtoplay
	 */
	public void playSound(Player player, String soundtoplay)
	{
		if (knownSounds.containsKey(soundtoplay))
		{
			player.playSound(knownSounds.get(soundtoplay), player.getPosition());
		}
	}

	/** The known sounds. */
	private HashMap<String, SoundAsset> knownSounds;
}
