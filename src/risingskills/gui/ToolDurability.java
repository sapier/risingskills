/*
 *
 */
package risingskills.gui;

import net.risingworld.api.events.EventMethod;
import net.risingworld.api.events.Listener;
import net.risingworld.api.events.general.UpdateEvent;
import net.risingworld.api.objects.Item;
import net.risingworld.api.objects.Player;
import net.risingworld.api.ui.UIElement;
import net.risingworld.api.ui.style.Pivot;

// TODO: Auto-generated Javadoc
/**
 * The Class ToolDurability.
 */
public class ToolDurability extends UIElement implements Listener {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3732083292277976895L;

	/** The Constant attrID. */
	public static final String attrID = "RISING_SKILLS_TOOL_DURABILITY";

	/**
	 * Instantiates a new notification bar.
	 *
	 * @param player the player
	 */
	public ToolDurability(Player player)
	{
		owner = player;

		setPosition(99.2f, 0.9f, true);
		setSize(75, 6, false);
		setBackgroundColor(1, 1, 1, (float) 0.5);
		setOpacity(1);
		setPivot(Pivot.UpperRight);

		fill = new UIElement();
		fill.setPosition(0, 0,  true);
		fill.setPivot(Pivot.UpperLeft);
		fill.setBackgroundColor(1, 0, 0, (float) 0.9);
		fill.setOpacity(1);
		fill.setSize(100, 100, true);

		addChild(fill);

		owner.addUIElement(this);
	}

	/** The owner. */
	private Player owner;

	/** The fill. */
	private UIElement fill;

	/**
	 * Update bar fill.
	 */
	private void updateBarFill()
	{
		Item equippedItem = owner.getEquippedItem();

		if (equippedItem != null)
		{
			int max_durability = equippedItem.getDefinition().durability;
			int cur_durability = equippedItem.getDurability();

			if (cur_durability < max_durability)
			{
				float red = 0.0f;
				float green = 0.0f;
				float blue = 0.0f;

				// > 50% == green
				if ( cur_durability * 2  > max_durability)
				{
					green = 1.0f;
				}
				// > 25% == yellow
				else if (cur_durability * 4 > max_durability)
				{
					green = 1.0f;
					red = 1.0f;
				}
				// < 25% == red
				else {
					red = 1.0f;
				}

				fill.setBackgroundColor(red, green, blue, 1);
				fill.setSize((cur_durability* 100) / max_durability, 100, true);
				setVisible(true);
			}
			else {
				setVisible(false);
			}
		}
		else {
			setVisible(false);
		}
	}

	/**
	 * On update event.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onUpdateEvent(UpdateEvent event)
	{
		updateBarFill();
	}
}
