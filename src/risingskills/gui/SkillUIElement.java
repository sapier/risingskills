/*
 * 
 */
package risingskills.gui;

import net.risingworld.api.ui.UIElement;
import net.risingworld.api.ui.UILabel;
import net.risingworld.api.ui.style.Font;
import net.risingworld.api.ui.style.Pivot;

// TODO: Auto-generated Javadoc
/**
 * The Class SkillUIElement.
 * < ------------------------------      width     ------------------------------------> *
 * ##################################################################################### *
 * # Skillname                                                                  <num>  # *
 * # -----------------------------------                                               # *
 * #                                     (x / Y )                                      # *
 * ##################################################################################### *
 */
public class SkillUIElement
{
	/** The container. */
	private UIElement container;

	/** The Skill text. */
	private UILabel SkillText;

	/** The Skill level. */
	private UILabel SkillLevel;

	/** The Skill progress. */
	private UILabel SkillProgress;

	/** The Skill progress bar. */
	private UIElement SkillProgressBarTotal;

	/** The Skill progress bar fill. */
	private UIElement SkillProgressBarFill;

	/** The Skill description. is expected to be used later*/
	@SuppressWarnings("unused")
	private String SkillDescription;

	/** The Skill name. */
	private String SkillName;

	/** The xp in next level. */
	int xpInNextLevel;

	/** The xp required next level. */
	int xpForNextLevel;

	/**
	 * Skill UI element.
	 *
	 * @param name the name
	 * @param description the description
	 */
	public SkillUIElement(String name, String description)
	{
		SkillName = name;
		SkillDescription = description;

		/* create the ui elements */
		container = new UIElement();
		SkillText = new UILabel(SkillName);
		SkillLevel = new UILabel("0");
		SkillProgress = new UILabel("(0/0)");
		SkillProgressBarTotal = new UIElement();
		SkillProgressBarFill = new UIElement();

		/* set container bgcolor (just for debugging */
		//container.setBackgroundColor(0, 0, 0, 1);
		//container.setOpacity(1);
		container.setPivot(Pivot.UpperLeft);
		container.setBackgroundColor(1, 1, 1, (float) 0.005);
		container.setOpacity(1);

		/* set correct positions and sizes */
		SkillText.setPosition(2, 0, true);
		SkillText.setPivot(Pivot.UpperLeft);
		SkillText.setFontSize(20);
		SkillText.setFont(Font.DefaultBold);

		SkillLevel.setPosition(98, 0, true);
		SkillLevel.setPivot(Pivot.UpperRight);
		SkillLevel.setFontSize(20);
		SkillLevel.setFont(Font.DefaultBold);

		SkillProgress.setPosition(50, 100, true);
		SkillProgress.setPivot(Pivot.LowerCenter);

		SkillProgressBarTotal.setPosition(50, 67, true);
		SkillProgressBarTotal.setSize(96, 10, true);
		SkillProgressBarTotal.setBackgroundColor(1, 1, 1, (float) 0.1);
		SkillProgressBarTotal.setOpacity(1);
		SkillProgressBarTotal.setPivot(Pivot.LowerCenter);

		SkillProgressBarFill.setPosition(0, 0,  true);
		SkillProgressBarFill.setPivot(Pivot.UpperLeft);
		SkillProgressBarFill.setBackgroundColor(1, 1, 1, (float) 0.9);
		SkillProgressBarFill.setOpacity(1);
		SkillProgressBarFill.setSize(66, 100, true);

		/* attach elements to container */
		container.addChild(SkillText);
		container.addChild(SkillLevel);
		container.addChild(SkillProgressBarTotal);
		SkillProgressBarTotal.addChild(SkillProgressBarFill);
		container.addChild(SkillProgress);

		container.updateStyle();
	}

	/**
	 * Ui element.
	 *
	 * @return the UI element
	 */
	public UIElement uiElement()
	{ return container; }

	/**
	 * Sets the width.
	 *
	 * @param width the width
	 */
	public void setWidth(float width)
	{
		container.setSize(width, 50, false);
		container.updateStyle();
	}

	/**
	 * Sets the position.
	 *
	 * @param x the x
	 * @param y the y
	 * @param percent the percent
	 */
	public void setPosition(float x, float y, boolean percent)
	{
		container.setPosition(x,  y,  percent);
	}

	/**
	 * Sets the level.
	 *
	 * @param toset the new level
	 */
	public void setLevel(int toset)
	{ SkillLevel.setText(String.valueOf(toset)); }

	/**
	 * Sets the x pfor next level.
	 *
	 * @param toset the new x pfor next level
	 */
	public void setXPForLevel(int toset)
	{
		xpForNextLevel = toset;
		SkillProgress.setText("(" + String.valueOf(xpInNextLevel) + "/" + xpForNextLevel + ")");
		drawProgressBar();
	}

	/**
	 * Sets the x pfor next level total.
	 *
	 * @param toset the new x pfor next level total
	 */
	public void setXPInLevel(int toset)
	{
		xpInNextLevel = toset;
		SkillProgress.setText("(" + String.valueOf(xpInNextLevel) + "/" + xpForNextLevel + ")");
		drawProgressBar();
	}


	/**
	 * Draw progress bar.
	 */
	private void drawProgressBar()
	{
		SkillProgressBarFill.setSize(100 * xpInNextLevel/xpForNextLevel, 100, true);
		container.updateStyle();
	}
}
