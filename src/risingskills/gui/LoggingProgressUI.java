/*
 *
 */
package risingskills.gui;

import net.risingworld.api.Timer;
import net.risingworld.api.objects.Player;
import net.risingworld.api.ui.UIElement;
import net.risingworld.api.ui.style.Pivot;

// TODO: Auto-generated Javadoc
/**
 * The Class LoggingProgressUI.
 */
public class LoggingProgressUI extends UIElement implements Runnable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3333143736542091406L;

	/**
	 * Instantiates a new logging progress UI.
	 *
	 * @param player the player
	 */
	public LoggingProgressUI(Player player)
	{
		owner = player;
		visible = true;

		setPosition(50, 90, true);
		setSize(30, 0.5f, true);
		setBackgroundColor(1, 1, 1, (float) 0.2);
		setOpacity(1);
		setPivot(Pivot.LowerCenter);

		ProgressBarFill = new UIElement();
		ProgressBarFill.setPosition(0, 0,  true);
		ProgressBarFill.setPivot(Pivot.UpperLeft);
		ProgressBarFill.setBackgroundColor(1, 1, 1, (float) 0.9);
		ProgressBarFill.setOpacity(1);
		ProgressBarFill.setSize(66, 100, true);

		addChild(ProgressBarFill);
	}

	/**
	 * Run.
	 */
	@Override
	public void run() {
		// TODO Auto-generated method stub
		visible = false;
		damageCollected = 0;
		healthTotal = 0;
		setVisible(visible);
		updateStyle();
		System.out.println("Hiding Logging Progress bar");
	}

	/**
	 * Show.
	 *
	 * @param damage_done the damage done
	 * @param total_damage the total damage
	 */
	public void show(int damage_done, int total_damage)
	{
		if (healthTotal != total_damage)
		{
			damageCollected = 0;
			healthTotal = total_damage;
		}

		damageCollected += damage_done;
		int sizepercent = (damageCollected * 100) / healthTotal;

		show(sizepercent);
	}

	/**
	 * Show.
	 *
	 * @param percent the percent
	 */
	public void show(int percent)
	{
		visible = true;
		setVisible(visible);

		if ((hideLoggingProgressTimer != null) && (!hideLoggingProgressTimer.isKilled()))
		{
			hideLoggingProgressTimer.kill();
		}
		hideLoggingProgressTimer = new Timer(1.0f,0,0,this);
		hideLoggingProgressTimer.start();

		ProgressBarFill.setSize(percent, 100, true);
		ProgressBarFill.setVisible(visible);

		// strange case just hide it for now
		if (damageCollected > healthTotal)
		{
			System.out.println("Something is wrong let's hide the bar");
			visible = false;
			setVisible(visible);
		}
		updateStyle();
		System.out.println("Logging Progress bar show: " + damageCollected + "/" + healthTotal + " " + percent + "%");
	}

	/** The hide logging progress timer. */
	private Timer hideLoggingProgressTimer;

	/** The owner. */
	@SuppressWarnings("unused")
	private Player owner;

	/** The visible. */
	private boolean visible;

	/** The damage collected. */
	private int damageCollected;

	/** The health total. */
	private int healthTotal;

	/** The progress bar fill. */
	private UIElement ProgressBarFill;
}
