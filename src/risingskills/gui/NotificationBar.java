/*
 *
 */
package risingskills.gui;

import net.risingworld.api.Timer;
import net.risingworld.api.objects.Player;
import net.risingworld.api.ui.UIElement;
import net.risingworld.api.ui.UILabel;
import net.risingworld.api.ui.style.Font;
import net.risingworld.api.ui.style.Pivot;

// TODO: Auto-generated Javadoc
/**
 * The Class NotificationBar.
 */
public class NotificationBar extends UIElement implements Runnable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1325997466430473315L;

	/** The Constant attrID. */
	public static final String attrID = "RISING_SKILLS_PLAYER_NOTIFICATION_AREA";

	/**
	 * Run.
	 */
	@Override
	public void run()
	{
		int showtime = 3;
		timePassed += timerInverval;

		if (timePassed < showtime)
		{
			return;
		}

		float opacity = 1 - (timePassed - showtime) / 1;
		notificationText.setOpacity(opacity);
	}

	/**
	 * Instantiates a new notification bar.
	 *
	 * @param player the player
	 */
	public NotificationBar(Player player)
	{
		owner = player;
		timerInverval = 0.15f;

		player.setAttribute(attrID, this);

		setPosition(50, 0, true);
		setSize(40, 4f, true);
		setBackgroundColor(0, 0, 0, (float) 0.0);
		setOpacity(0);
		setPivot(Pivot.UpperCenter);

		notificationText = new UILabel();
		notificationText.setPosition(50,100, true);
		notificationText.setPivot(Pivot.LowerCenter);
		notificationText.setFontColor(1,1,1,1);
		notificationText.setFont(Font.DefaultBold);
		notificationText.setOpacity(1);
		notificationText.setFontSize(25);
		notificationText.setText("SOME REALLY STUPID TEST STRING");
		addChild(notificationText);

		owner.addUIElement(this);
	}

	/**
	 * Show message.
	 *
	 * @param message the message
	 */
	public void showMessage(String message)
	{
		notificationText.setOpacity(1);
		notificationText.setText(message);

		// TODO start timer
		if ((visibilityTimer != null) && (!visibilityTimer.isKilled()))
		{
			visibilityTimer.kill();
		}
		visibilityTimer = new Timer(timerInverval, 0, -1, this);
		visibilityTimer.start();

		timePassed = 0;
	}

	/** The owner. */
	private Player owner;

	/** The notification text. */
	private UILabel notificationText;

	/** The visibility timer. */
	private Timer visibilityTimer;

	/** The timer inverval. */
	float timerInverval;

	/** The time passed. */
	float timePassed;
}
