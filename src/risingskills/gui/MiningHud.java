/*
 *
 */
package risingskills.gui;

import net.risingworld.api.events.EventMethod;
import net.risingworld.api.events.Listener;
import net.risingworld.api.events.general.UpdateEvent;
import net.risingworld.api.objects.Player;
import net.risingworld.api.ui.UIElement;
import net.risingworld.api.ui.UILabel;
import net.risingworld.api.ui.style.Pivot;

// TODO: Auto-generated Javadoc
/**
 * The Class MiningHud.
 */
public class MiningHud extends UIElement implements Listener
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -9137499405575848956L;

	/** The attr ID. */
	private static String attrID = "RISING_SKILLS_MINING_HUD";

	/**
	 * The Enum MiningHudFeatureLevel.
	 */
	public enum MiningHudFeatureLevel {

		/** The None. */
		None,

		/** The Azimutth. */
		Azimuth,

		/** The Direction. */
		Direction,

		/** The Elevation. */
		Elevation,

		/** The Undefined. */
		Undefined
	}

	/**
	 * Instantiates a new mining hud.
	 *
	 * @param player the player
	 */
	public MiningHud(Player player)
	{
		owner = player;
		currentFeatureLevel = MiningHudFeatureLevel.None;
		isVisible = false;

		if (player.getAttribute(attrID) != null)
		{
			System.out.println("Player " + owner.getName() + " does already have a mining hud, not adding this one");
			return;
		}

		owner.setAttribute(attrID, this);

		setPosition(50,50, true);
		setBackgroundColor(0, 0, 0, 0);
		setOpacity((float) 0.0);
		setPivot(Pivot.MiddleCenter);
		setSize(10, 10, true);

		String angle = "90°";
		azimuth = new UILabel(angle);
		azimuth.setPosition(100, 50,  true);
		azimuth.setPivot(Pivot.MiddleCenter);
		azimuth.setFontSize(12);
		azimuth.setVisible(false);
		addChild(azimuth);


		String label_dir = "180° NW";
		direction = new UILabel(label_dir);
		direction.setPosition(50, 0,  true);
		direction.setPivot(Pivot.UpperCenter);
		direction.setFontSize(12);
		direction.setVisible(false);
		addChild(direction);


		String label_elevation = "-205 m";
		elevation = new UILabel(label_elevation);
		elevation.setPosition(0, 0,  true);
		elevation.setPivot(Pivot.UpperLeft);
		elevation.setFontSize(12);
		elevation.setVisible(false);
		addChild(elevation);

		setVisible(isVisible);
		owner.addUIElement(this);
	}

	/**
	 * Sets the feature level.
	 *
	 * @param level the level
	 * @return the mining hud feature level
	 */
	public MiningHudFeatureLevel setFeatureLevel(MiningHudFeatureLevel level)
	{
		if (level != MiningHudFeatureLevel.Undefined)
		{
			currentFeatureLevel = level;
			updateVisibility();
		}

		return currentFeatureLevel;
	}

	/**
	 * Sets the visibility.
	 *
	 * @param toset the toset
	 * @return true, if successful
	 */
	public boolean setVisibility(boolean toset)
	{
		isVisible = toset;
		setVisible(isVisible);
		updateVisibility();
		return isVisible;
	}

	/**
	 * Sets the feature level.
	 *
	 * @param player the player
	 * @param level the level
	 * @return the mining hud feature level
	 */
	public static MiningHudFeatureLevel setFeatureLevel(Player player, MiningHudFeatureLevel level)
	{
		MiningHud toupdate = (MiningHud) player.getAttribute(attrID);

		if (toupdate == null)
		{
			return MiningHudFeatureLevel.Undefined;
		}

		return toupdate.setFeatureLevel(level);
	}

	/**
	 * Sets the visibility.
	 *
	 * @param player the player
	 * @param toset the toset
	 * @return true, if successful
	 */
	public static boolean setVisibility(Player player, boolean toset)
	{
		MiningHud toupdate = (MiningHud) player.getAttribute(attrID);

		if (toupdate == null)
		{
			return false;
		}

		return toupdate.setVisibility(toset);
	}

	/**
	 * On update event.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onUpdateEvent(UpdateEvent event)
	{
		if (!isVisible) {
			return;
		}

		switch (currentFeatureLevel)
		{
		case Elevation:
			elevation.setText(String.format("%.1f m", owner.getPosition().y));

		case Direction:
			direction.setText(String.format("%.0f°", owner.getHeading()));

		case Azimuth:
			float angel = 90 * owner.getViewDirection().y;
			azimuth.setText(String.format("%.0f°", angel));
			break;

		case None:
		default:
			break;
		}

	}

	/**
	 * Update visibility.
	 */
	private void updateVisibility()
	{
		if (isVisible == false)
		{
			azimuth.setVisible(false);
			direction.setVisible(false);
			elevation.setVisible(false);
			return;
		}

		switch (currentFeatureLevel)
		{
		case Elevation:
			elevation.setVisible(true);
			direction.setVisible(true);
			azimuth.setVisible(true);
			break;

		case Direction:
			direction.setVisible(true);
			azimuth.setVisible(true);
			break;

		case Azimuth:
			azimuth.setVisible(true);
			break;

		case None:
			azimuth.setVisible(false);
			direction.setVisible(false);
			elevation.setVisible(false);
			break;

		default:
			azimuth.setVisible(false);
			direction.setVisible(false);
			elevation.setVisible(false);
			break;

		}
	}

	/** The owner. */
	Player owner;

	/** The azimuth. */
	UILabel azimuth;

	/** The direction. */
	UILabel direction;

	/** The elevation. */
	UILabel elevation;

	/** The current feature level. */
	MiningHudFeatureLevel currentFeatureLevel;

	/** The is visible. */
	boolean isVisible;
}
