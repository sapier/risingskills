/*
 *
 */
package risingskills.gui;

import net.risingworld.api.Timer;
import net.risingworld.api.definitions.Crafting.Recipe;
import net.risingworld.api.objects.Player;
import net.risingworld.api.ui.UIElement;
import net.risingworld.api.ui.UILabel;
import net.risingworld.api.ui.style.Font;
import net.risingworld.api.ui.style.Pivot;
import risingskills.CraftingSpec;
import risingskills.SkillManager;
import risingskills.SkillManager.SkillEnum;
import risingskills.skills.DefaultCraftingSkill;

// TODO: Auto-generated Javadoc
/**
 * The Class LoggingProgressUI.
 */
public class CraftingProgress extends UIElement implements Runnable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 425295557261685367L;

	/**
	 * Instantiates a new logging progress UI.
	 *
	 * @param player the player
	 * @param manager the manager
	 * @param skill the skill
	 */
	public CraftingProgress(Player player, SkillManager manager, SkillEnum skill)
	{
		owner = player;
		skillManager = manager;
		connectedSkill = skill;
		visible = false;
		setVisible(false);

		setPosition(48, 85, true);
		setSize(30, 2f, true);
		setBackgroundColor(1, 1, 1, (float) 0.3);
		setOpacity(1);
		setPivot(Pivot.LowerCenter);

		ProgressBarFill = new UIElement();
		ProgressBarFill.setPosition(0, 0,  true);
		ProgressBarFill.setPivot(Pivot.UpperLeft);
		ProgressBarFill.setBackgroundColor(1, 1, 1, 1);
		ProgressBarFill.setOpacity(0.5f);
		addChild(ProgressBarFill);

		progressBarText = new UILabel();
		progressBarText.setPosition(50,100, true);
		progressBarText.setPivot(Pivot.LowerCenter);
		progressBarText.setFontColor(0,0,0,1);
		progressBarText.setFont(Font.DefaultBold);
		progressBarText.setOpacity(1);
		addChild(progressBarText);
	}

	/**
	 * Restart timer.
	 *
	 * @param time the time
	 */
	private void restartTimer(float time)
	{
		if ((updateProgressTimer != null) && (!updateProgressTimer.isKilled()))
		{
			updateProgressTimer.kill();
		}
		updateProgressTimer = new Timer(time,0,0,this);
		updateProgressTimer.start();
	}

	/**
	 * Run.
	 */
	@Override
	public void run() {
		float sleeptime = getSleepTime();

		timePassed += sleeptime;

		if (timePassed < activeCraftingSpec.craftingTime)
		{
			restartTimer(sleeptime);
			int percent = (int) Math.round((int) (timePassed*100) / activeCraftingSpec.craftingTime);
			ProgressBarFill.setSize(percent, 100, true);
			updateStyle();
		}
		else {
			if ((activeCraftingSpec == null) || (activeRecipe == null))
			{
				amountToCraft = 0;
				return;
			}

			amountToCraft -= 1;

			DefaultCraftingSkill craftingskill =  (DefaultCraftingSkill) skillManager.getSkill(connectedSkill);
			craftingskill.craftingFinished(owner, activeRecipe, activeCraftingSpec);

			if (amountToCraft > 0)
			{
				// check if the conditions are suitable for continuation of crafting
				if (craftingskill.startCrafting(owner, activeRecipe) == false)
				{
					String i18nformat = skillManager.geti18nFormat("NOT_ENOUGH_RESOURCES_TO_CRAFT", owner);
					skillManager.notifyPlayer(owner, String.format(i18nformat, activeRecipe.name));
					amountToCraft = 0;
				}
				else
				{
					String i18nformat = skillManager.geti18nFormat("CRAFTING_PROGRESSBAR_LABEL", owner);
					String label = String.format(i18nformat, activeRecipe.name, String.valueOf(initialAmountToCraft - amountToCraft + 1), String.valueOf(initialAmountToCraft));
					progressBarText.setText(label);

					timePassed = 0;
					restartTimer(sleeptime);
					ProgressBarFill.setSize(100, 100, true);
					updateStyle();
				}
			}
			if (amountToCraft <= 0)
			{
				visible = false;
				setVisible(visible);
				updateStyle();
			}
		}
	}

	/**
	 * Show.
	 *
	 * @param recipe the recipe
	 * @param spec the spec
	 * @param amount the amount
	 */
	public void startCrafting(Recipe recipe, CraftingSpec spec, Integer amount)
	{
		activeCraftingSpec = spec;
		activeRecipe = recipe;
		initialAmountToCraft = amount;
		amountToCraft = initialAmountToCraft;
		timePassed = 0;
		show();
	}

	/**
	 * Abort crafting.
	 */
	public void abortCrafting()
	{
		activeRecipe = null;
		activeCraftingSpec = null;
		amountToCraft = null;

		if ((updateProgressTimer != null) && (!updateProgressTimer.isKilled()))
		{
			updateProgressTimer.kill();
		}

		visible = false;
		setVisible(visible);
		updateStyle();
	}

	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive()
	{
		return visible;
	}

	/**
	 * Gets the sleep time.
	 *
	 * @return the sleep time
	 */
	private float getSleepTime()
	{
		Double sleeptime = activeCraftingSpec.craftingTime / 20;

		return (float) Math.min(0.2, sleeptime);
	}

	/**
	 * Show.
	 */
	private void show()
	{
		visible = true;
		setVisible(visible);

		String i18nformat = skillManager.geti18nFormat("CRAFTING_PROGRESSBAR_LABEL", owner);
		String label = String.format(i18nformat, activeRecipe.name, String.valueOf(initialAmountToCraft - amountToCraft + 1), String.valueOf(initialAmountToCraft));
		progressBarText.setText(label);

		float sleeptime = getSleepTime();

		if ((updateProgressTimer != null) && (!updateProgressTimer.isKilled()))
		{
			updateProgressTimer.kill();
		}
		updateProgressTimer = new Timer(sleeptime,0,0,this);
		updateProgressTimer.start();

		ProgressBarFill.setSize(0, 100, true);
		ProgressBarFill.setVisible(visible);

		updateStyle();
	}

	/** The hide logging progress timer. */
	private Timer updateProgressTimer;

	/** The owner. */
	private Player owner;

	/** The visible. */
	private boolean visible;

	/** The skillManager instance. */
	private SkillManager skillManager;

	/** The connected skill. */
	private SkillEnum connectedSkill;

	/** The progress bar fill. */
	private UIElement ProgressBarFill;

	/** The progress bar text. */
	private UILabel progressBarText;

	/** The spec. */
	private CraftingSpec activeCraftingSpec;

	/** The active recipe. */
	private Recipe activeRecipe;

	/** The amount to craft. */
	private Integer amountToCraft;

	/** The amount to craft. */
	private Integer initialAmountToCraft;

	/** The time passed. */
	private float timePassed;
}
