/*
 *
 */
package risingskills.gui;

import java.util.HashMap;

import net.risingworld.api.events.EventMethod;
import net.risingworld.api.events.Listener;
import net.risingworld.api.events.player.ui.PlayerUIElementClickEvent;
import net.risingworld.api.objects.Player;
import net.risingworld.api.ui.UIElement;
import net.risingworld.api.ui.UILabel;
import net.risingworld.api.ui.style.Font;
import net.risingworld.api.ui.style.Pivot;
import net.risingworld.api.ui.style.TextAnchor;
import risingskills.SkillManager;
import risingskills.SkillManager.SkillEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class SkillUI.
 */
public class SkillUI extends UIElement implements Listener {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6483555868611550070L;

	/** The owner. */
	private Player owner;

	/** The skill manager. */
	private SkillManager skillManager;

	/** The header. */
	private UILabel header;

	/** The close button. */
	private UILabel closeButton;

	/** The skilltable. */
	private HashMap<String, SkillUIElement> skilltable;

	/** The visible. */
	private boolean visible;

	/**
	 * Update skills.
	 */
	private void updateSkills()
	{
		for (SkillEnum skill : SkillEnum.values())
		{
			if (skill != SkillEnum.RISING_SKILLS_CRAFTING_DEFAULT)
			{
				int experience = (int) owner.getAttribute(skill.name());
				int level = risingskills.SkillLevelHelper.getSkillLevel(skill, experience);

				skilltable.get(skill.name()).setLevel(level);
				skilltable.get(skill.name()).setXPForLevel(risingskills.SkillLevelHelper.getXPForLevel(skill, level));
				skilltable.get(skill.name()).setXPInLevel(risingskills.SkillLevelHelper.getXPInLevel(skill, experience));
			}
		}
	}

	/**
	 * Instantiates a new skill UI.
	 *
	 * @param player the player
	 * @param manager the manager
	 */
	public SkillUI(Player player, SkillManager manager) {
		float main_width = 500;
		float main_height = 800;
		owner = player;
		skillManager = manager;

		skillManager.registerEventListener(this);

		setBackgroundColor(0, 0, 0, 1);
		setOpacity((float) 0.8);
		setPosition(50,50,true);
		setPivot(Pivot.MiddleCenter);
		setSize(main_width, main_height, false);

		// TODO get localized header
		String localizedHeader = "Skills";
		header = new UILabel(localizedHeader);
		header.setPosition(50, 10,  true);
		header.setPivot(Pivot.MiddleCenter);
		header.setFontSize(25);
		header.setFont(Font.DefaultBold);
		addChild(header);

		closeButton = new UILabel("X");
		closeButton.setPosition(100, 0,  true);
		closeButton.setPivot(Pivot.UpperRight);
		closeButton.setFontSize(25);
		closeButton.setFont(Font.DefaultBold);
		closeButton.setTextAlign(TextAnchor.MiddleCenter);
		closeButton.setSize(30,30, false);
		closeButton.setFontColor(0,0,0,1);
		closeButton.setClickable(true);
		closeButton.setBackgroundColor(44, 44, 44, 1);
		addChild(closeButton);

		skilltable = new HashMap<String, SkillUIElement>();

		int relativeposition = 20;
		for (SkillEnum skill : SkillEnum.values())
		{
			// this is a special skill just for convenience purposes not supposed to accumulate experience anyway
			if (skill == SkillEnum.RISING_SKILLS_CRAFTING_DEFAULT) {
				continue;
			}

			if (relativeposition > 100)
			{
				System.out.println("UI not big enough for all skills, the developer needs to do something about this");
				break;
			}
			// get localized name for skill
			String localized_skillname = manager.geti18nFormat(skill.name() + "_TITLE", player);

			// TODO get localized description for skill
			String localized_skilldesciption = "";

			SkillUIElement toadd = new SkillUIElement(localized_skillname, localized_skilldesciption);
			skilltable.put(skill.name(), toadd);

			toadd.setPosition(10, relativeposition, true);
			toadd.setWidth((float) (main_width * 0.8));
			addChild(toadd.uiElement());

			relativeposition += 8;
		}

		visible = false;
		setVisible(visible);
		owner.addUIElement(this);
	}

	/**
	 * Hide.
	 */
	public void hide()
	{
		visible = false;
		setVisible(visible);
		owner.setMouseCursorVisible(visible);
	}

	/**
	 * Toggle.
	 */
	public void toggle()
	{
		if (visible) {
			visible = false;
		}
		else {
			visible = true;
			updateSkills();
		}
		owner.setMouseCursorVisible(visible);
		setVisible(visible);
	}

	/**
	 * On click.
	 *
	 * @param event the event
	 */
	@Override
	@EventMethod
	public void onClick(PlayerUIElementClickEvent event)
	{
		if (event.getUIElement() == closeButton)
		{
			toggle();
		}
	}
}
