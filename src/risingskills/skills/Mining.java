/*
 *
 */
package risingskills.skills;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;

import net.risingworld.api.Server;
import net.risingworld.api.definitions.Items;
import net.risingworld.api.definitions.Plants;
import net.risingworld.api.definitions.Plants.TerrainMaterialType;
import net.risingworld.api.events.Event;
import net.risingworld.api.events.EventMethod;
import net.risingworld.api.events.Listener;
import net.risingworld.api.events.player.PlayerChangeEquippedItemEvent;
import net.risingworld.api.events.player.PlayerSpawnEvent;
import net.risingworld.api.events.player.world.PlayerDestroyTerrainEvent;
import net.risingworld.api.events.player.world.PlayerHitTerrainEvent;
import net.risingworld.api.events.player.world.PlayerHitVegetationEvent;
import net.risingworld.api.events.player.world.PlayerRemoveVegetationEvent;
import net.risingworld.api.objects.Item;
import net.risingworld.api.objects.Player;
import net.risingworld.api.objects.world.ChunkPart;
import net.risingworld.api.objects.world.Plant;
import risingskills.SkillLevelHelper;
import risingskills.SkillManager;
import risingskills.SkillManager.SkillEnum;
import risingskills.gui.LoggingProgressUI;
import risingskills.gui.MiningHud;
import risingskills.gui.MiningHud.MiningHudFeatureLevel;

// TODO: Auto-generated Javadoc
/**
 * The Class Mining.
 */
public class Mining implements Listener
{

	/** The Progress UI attibute ID. */
	private String ProgressUIAttibuteID;

	/**
	 * The Class Tool.
	 */
	private class Tool
	{

		/** The name. */
		@SuppressWarnings("unused")
		public String name;

		/** The shoveltier. */
		public int shoveltier;

		/** The pickaxetier. */
		public int pickaxetier;

		/** The supports mining hud. */
		public boolean supportsMiningHud;
	}

	/**
	 * The Enum TerrainType.
	 */
	private enum TerrainType
	{

		/** The Unnown. */
		Unnown,

		/** The Solid. */
		Solid,

		/** The Sand. */
		Sand
	}

	/**
	 * The Class Terrain.
	 */
	private class Terrain
	{

		/** The name. */
		@SuppressWarnings("unused")
		public String name;

		/** The hardness. */
		public int hardness;

		/** The type. */
		public TerrainType type;
	}

	/**
	 * Instantiates a new Mining skill instance.
	 *
	 * @param manager the manager
	 * @param saveimmediatelly the saveimmediatelly
	 */
	public Mining(SkillManager manager, boolean saveimmediatelly)
	{
		immediateSave = saveimmediatelly;
		skillManager = manager;
		ProgressUIAttibuteID = "RISING_SKILLS_MINING_PROGRESS";
		rockhardness = new HashMap<String, Integer>();
		tools = new HashMap<String, Tool>();
		terrains = new HashMap<String, Terrain>();

		// TODO get localized name
		skillName = "Mining";
		skill = SkillEnum.RISING_SKILLS_MINING;

		String path = manager.getPath() + "/mining.yml";
		try {
			Yaml yaml = new Yaml();
			String yamlraw = new String(Files.readAllBytes(Paths.get(path)));
			config = (Map<String, Object>) yaml.load(yamlraw);

			baseLearningAmount = (Integer) config.get("baseLearnAmount");
			SkillLevelHelper.setBaseXPAmount(skill, (Integer) config.get("baseXPAmount"));

			validateToolTierConfiguration();
			validateRockTierConfiguration();
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			e.printStackTrace();
			System.out.println(skillManager.getName() + ": \"" + path + "\" isn't a valid json file => no mining skill available");
		}
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return skillName;
	}

	/** The immediate save. */
	private boolean immediateSave;

	/** The skill manager. */
	private SkillManager skillManager;

	/** The config. */
	private Map<String, Object> config;

	/** The tool tier. */
	private HashMap<String, Tool> tools;

	/** The base learing amount. */
	private Integer baseLearningAmount;

	/** The hardness. */
	private HashMap<String, Integer> rockhardness;

	/** The terrains. */
	private HashMap<String, Terrain> terrains;

	/** The last tool tier warning. */
	private float lastToolTierWarning = 0;

	/** The skill. */
	SkillEnum skill;

	/** The skill name. */
	private String skillName;

	/**
	 * On player hit terrain event.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onPlayerHitTerrainEvent(PlayerHitTerrainEvent event)
	{
		processMiningEvent(event);
	}

	/**
	 * On player destroy terrain event.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onPlayerDestroyTerrainEvent(PlayerDestroyTerrainEvent event)
	{
		processMiningEvent(event);
	}

	/**
	 * On vegetation hit.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onVegetationHit(PlayerHitVegetationEvent event)
	{
		processMiningEvent(event);
	}

	/**
	 * On remove vegetation.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onRemoveVegetation(PlayerRemoveVegetationEvent event)
	{
		processMiningEvent(event);
	}


	/**
	 * On player spawn.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onPlayerSpawn(PlayerSpawnEvent event)
	{
		Player player = event.getPlayer();

		// adding mining hud
		MiningHud newhud = new MiningHud(player);
		skillManager.registerEventListener(newhud);

		int currentSkillExperience = (int) player.getAttribute(skill.name());
		int level = risingskills.SkillLevelHelper.getSkillLevel(skill, currentSkillExperience);
		updateMiningHudLevel(player, level);

		if (player.getEquippedItem() == null)
		{
			newhud.setVisibility(false);
			return;
		}
		String equippedName = player.getEquippedItem().getName();
		if (tools.containsKey(equippedName))
		{
			if (tools.get(equippedName).supportsMiningHud)
			{
				newhud.setVisibility(true);
			}
			else {
				newhud.setVisibility(false);
			}
		}
	}

	/**
	 * On player change equipped item event.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onPlayerChangeEquippedItemEvent(PlayerChangeEquippedItemEvent event)
	{
		if (event.getItem() == null)
		{
			MiningHud.setVisibility(event.getPlayer(), false);
			return;
		}

		String equippedName = event.getItem().getName();
		if (tools.containsKey(equippedName))
		{
			if (tools.get(equippedName).supportsMiningHud)
			{
				MiningHud.setVisibility(event.getPlayer(), true);
			}
			else {
				MiningHud.setVisibility(event.getPlayer(), false);
			}
		}
	}

	/**
	 * Calculate experience.
	 *
	 * @param baseXP the base XP
	 * @param skilllevel the skilllevel
	 * @param interactionobject the interactionobject
	 * @param damagefraction the damagefraction
	 * @return the int
	 */
	private int calculateExperience(int baseXP, int skilllevel, int interactionobject, double damagefraction)
	{
		int leveldiff = skilllevel - interactionobject;

		// as we're using a division to adjust the xp we have to increas eit by one to get
		// base xp on no level difference
		leveldiff += 1;

		// full xp if the player is not yet at the given level
		// ... well not exactly it's gonna be reduced by the damagefragtion later
		if (leveldiff < 1)
		{
			leveldiff = 1;
		}

		// the higher the level difference the less xp you're gonna get
		int resultingxp = Math.floorDiv(baseXP, leveldiff);

		// based on tool and player level the damage is scaled this may have a negative effect on experience too
		resultingxp *= Math.min(damagefraction, 1);

		// let's be nice and give at least 1 xp
		resultingxp = Math.max(1, resultingxp);
		//System.out.println("calculateExperience: base="  + baseXP + " calculated=" + resultingxp + " skillevel=" + skilllevel + " planttier=" + planttier + " damagefraction=" + damagefraction);
		return resultingxp;
	}

	/**
	 * Calculate damage.
	 *
	 * @param player the player
	 * @param basedamage the basedamage
	 * @param skillevel the skillevel
	 * @param objecttier the objecttier
	 * @param tooltier the tier the wielded tool is at
	 * @return the double
	 */
	private short calculateDamage(Player player, short basedamage, int skillevel, int objecttier, int tooltier)
	{
		// tiers smaller 0 mean the tool can't damage this
		if (tooltier < 0) {
			return 0;
		}

		// reduce damage drastically if tool isn't suitable for rock
		if (objecttier > tooltier)
		{
			//System.out.println("calculateDamage: tool not sufficient reduce damage drastically " + tooltier + " < " + objecttier);
			basedamage *= 0.5;
			float now = Server.getRunningTime();

			if (now > lastToolTierWarning + 10)
			{
				String i18nformat = skillManager.geti18nFormat("TOOL_TIER_INSUFFICIENT", player);
				skillManager.notifyPlayer(player, String.format(i18nformat));
			}
			lastToolTierWarning = now;
		}

		// shortcut for simple tools ... and to avoid division by 0 ;-)
		if (tooltier == 0) {
			//System.out.println("calculateDamage: tier 0 base="  + basedamage);
			return basedamage;
		}

		int resulting_damage = basedamage + basedamage * (skillevel/(tooltier*3));

		// slow down damage increase
		if (skillevel > 6* tooltier)
		{
			resulting_damage = basedamage + (int) Math.floor((2 * basedamage) + ((skillevel - 6) * 0.1* basedamage));
		}

		//System.out.println("calculateDamage: base="  + basedamage + " calculated=" + resulting_damage + " skillevel=" + skillevel + " tooltier=" + tooltier);
		return (short) resulting_damage;
	}

	/**
	 * Calculate wear.
	 *
	 * @param skillevel the skillevel
	 * @param obecttier the tier of the interacted object
	 * @param tooltier the tooltier
	 * @return the int
	 */
	private int calculateWear(int skillevel, int obecttier, int tooltier)
	{
		float wear = 2;

		if (obecttier > tooltier)
		{
			wear*=1.5;
		}

		float skilllevelcorrection = (float) Math.pow(0.9, (skillevel - obecttier));

		if (skillevel > obecttier)
		{
			wear *= skilllevelcorrection;
		}
		wear = Math.round(wear);
		System.out.println("Wear: skillcorrection=" + skilllevelcorrection + " objecttier=" + obecttier + " tooltier=" + tooltier + " wear=" + wear);
		return (int) wear;
	}

	/**
	 * Gets the tool tier.
	 *
	 * @param name the name
	 * @param type the type
	 * @return the tool tier
	 */
	private int getToolTier(String name, TerrainType type)
	{
		if (type == TerrainType.Sand)
		{
			if (tools.containsKey(name))
			{
				return tools.get(name).shoveltier;
			}
		}
		else if (type == TerrainType.Solid)
		{
			if (tools.containsKey(name))
			{
				return tools.get(name).pickaxetier;
			}
		}

		return -1;
	}

	/**
	 * Gets the object tier.
	 *
	 * @param object the object interacted with
	 * @return the tier of the object
	 */
	private int getObjectTier(Plants.PlantDefinition object)
	{
		// find hardness
		int hardnessvalue = 0;
		boolean found = false;
		for (String key : rockhardness.keySet())
		{
			if (object.name.startsWith(key, 0)) {
				hardnessvalue = rockhardness.get(key);
				found = true;
				break;
			}
		}

		if (!found)
		{
			System.out.println(skillManager.getName() + ": missing hardness configuration for " + object.name);
		}

		int tier = Math.floorDiv(hardnessvalue, 500);
		return tier;
	}

	/**
	 * Gets the terrain tier.
	 *
	 * @param terrainname the terrainname
	 * @return the terrain tier
	 */
	private int getTerrainTier(String terrainname)
	{
		// find hardness
		int hardnessvalue = 0;
		boolean found = false;
		for (String key : terrains.keySet())
		{
			if (terrainname.startsWith(key, 0)) {
				hardnessvalue = terrains.get(key).hardness;
				found = true;
				break;
			}
		}

		if (!found)
		{
			System.out.println(skillManager.getName() + ": missing hardness configuration for terrain " + terrainname);
		}

		int tier = Math.floorDiv(hardnessvalue, 500);

		return tier;
	}

	/**
	 * Gets the terrain type.
	 *
	 * @param terrainname the terrainname
	 * @return the terrain type
	 */
	private TerrainType getTerrainType(String terrainname)
	{
		if (terrains.containsKey(terrainname))
		{
			return terrains.get(terrainname).type;
		}

		return TerrainType.Unnown;
	}

	/**
	 * Checks if is pickable mineral.
	 *
	 * @param object the object
	 * @return true, if is pickable mineral
	 */
	private boolean isPickableMineral(Plants.PlantDefinition object)
	{
		if (rockhardness.containsKey(object.name))
		{
			return true;
		}
		return false;
	}

	/**
	 * Process mining event.
	 *
	 * @param event the event
	 */
	private void processMiningEvent(Event event)
	{
		if (config == null)
		{
			return;
		}

		int levelbefore = 0;
		int levelafter = 0;
		Player player = null;

		if (event instanceof PlayerHitTerrainEvent)
		{
			PlayerHitTerrainEvent evt = (PlayerHitTerrainEvent) event;
			ChunkPart chunkpart = net.risingworld.api.World.getChunkPart(evt.getChunkPositionX(), evt.getChunkPositionY(), evt.getChunkPositionZ());
			int terrainID = chunkpart.getTerrainID(evt.getBlockPositionX(), evt.getBlockPositionY(), evt.getBlockPositionZ());
			TerrainMaterialType materialtype = Plants.TerrainMaterialType.get(terrainID);
			if (materialtype == null)
			{
				System.out.println(skillManager.getName() + ":  Mining processMiningEvent hit terrain. TerrainID: " + terrainID + " doesn't have a material type");
				evt.setCancelled(true);
				return;
			}
			TerrainType terraintype = getTerrainType(materialtype.name());
			int terrainttier = getTerrainTier(materialtype.name());
			player = evt.getPlayer();
			int currentSkillExperience = (int) player.getAttribute(skill.name());
			levelbefore = risingskills.SkillLevelHelper.getSkillLevel(skill, currentSkillExperience);
			Item tool = player.getEquippedItem();
			short initial_damage = evt.getDamage();
			int currentDurability = tool.getDurability();

			if (currentDurability <= 0)
			{
				evt.setCancelled(true);

				String i18nformat = skillManager.geti18nFormat("TOOL_BROKEN_WARNING", player);
				skillManager.notifyPlayer(player, String.format(i18nformat, tool.getName()));
				return;
			}

			int tooltier = getToolTier(tool.getName(), terraintype);
			short final_damage = calculateDamage(player, initial_damage, levelbefore, terrainttier, tooltier);
			int experiencegain = calculateExperience(baseLearningAmount, levelbefore, terrainttier, (float) final_damage / (float) initial_damage);

			currentSkillExperience += experiencegain;
			// setting damage is not supported
			//evt.setDamage(final_damage);
			player.setAttribute(skill.name(), currentSkillExperience);
			levelafter = risingskills.SkillLevelHelper.getSkillLevel(skill, currentSkillExperience);

			int wear = calculateWear(levelbefore, terrainttier, tooltier);
			tool.setDurability(Math.max(0, currentDurability-wear));

			System.out.println(skillManager.getName() + ":  Mining processMiningEvent hit terrain. TerrainID: " + terrainID + " mt: " + materialtype.name());
		}
		else if (event instanceof PlayerDestroyTerrainEvent)
		{
			PlayerDestroyTerrainEvent evt = (PlayerDestroyTerrainEvent) event;
			ChunkPart chunkpart = net.risingworld.api.World.getChunkPart(evt.getChunkPositionX(), evt.getChunkPositionY(), evt.getChunkPositionZ());
			int prevterrainID = chunkpart.getTerrainID(evt.getBlockPositionX(), evt.getBlockPositionY(), evt.getBlockPositionZ());
			TerrainMaterialType prevmaterialtype = Plants.TerrainMaterialType.get(prevterrainID);
			System.out.println(skillManager.getName() + ":  Mining processMiningEvent destroy terrain. prev: TerrainID: " + prevterrainID + " mt: " + prevmaterialtype.name());
			int terrainID = evt.getTerrainID();
			TerrainMaterialType materialtype = Plants.TerrainMaterialType.get(terrainID);
			TerrainType terraintype = getTerrainType(materialtype.name());
			int terrainttier = getTerrainTier(materialtype.name());
			player = evt.getPlayer();
			int currentSkillExperience = (int) player.getAttribute(skill.name());
			levelbefore = risingskills.SkillLevelHelper.getSkillLevel(skill, currentSkillExperience);
			Item tool = player.getEquippedItem();
			//short initial_damage = evt.getDamage();
			int currentDurability = tool.getDurability();

			if (currentDurability <= 0)
			{
				evt.setCancelled(true);
				String i18nformat = skillManager.geti18nFormat("TOOL_BROKEN_WARNING", player);
				skillManager.notifyPlayer(player, String.format(i18nformat, tool.getName()));
				return;
			}

			int tooltier = getToolTier(tool.getName(), terraintype);
			//short final_damage = calculateDamage(player, initial_damage, levelbefore, terrainttier, tooltier, currentDurability);
			int experiencegain = calculateExperience(baseLearningAmount, levelbefore, terrainttier, 1);

			currentSkillExperience += experiencegain;
			// setting damage is not supported
			//evt.setDamage(final_damage);
			player.setAttribute(skill.name(), currentSkillExperience);
			levelafter = risingskills.SkillLevelHelper.getSkillLevel(skill, currentSkillExperience);

			int wear = calculateWear(levelbefore, terrainttier, tooltier);
			tool.setDurability(Math.max(0, currentDurability - wear));
		}
		else if (event instanceof PlayerHitVegetationEvent)
		{
			PlayerHitVegetationEvent evt = (PlayerHitVegetationEvent) event;
			player = evt.getPlayer();
			int currentSkillExperience = (int) player.getAttribute(skill.name());
			levelbefore = risingskills.SkillLevelHelper.getSkillLevel(skill, currentSkillExperience);
			Plants.PlantDefinition def = evt.getPlantDefinition();

			if (def.type != Plants.Type.Rock)
			{
				return;
			}

			Item tool = player.getEquippedItem();
			short initial_damage = evt.getDamage();
			int currentDurability = tool.getDurability();

			if (currentDurability <= 0)
			{
				evt.setCancelled(true);
				String i18nformat = skillManager.geti18nFormat("TOOL_BROKEN_WARNING", player);
				skillManager.notifyPlayer(player, String.format(i18nformat, tool.getName()));
				return;
			}

			int tooltier = getToolTier(tool.getName(), TerrainType.Solid);
			int objecttier = getObjectTier(def);
			short final_damage = calculateDamage(player, initial_damage, levelbefore, objecttier, tooltier);
			int experiencegain = calculateExperience(baseLearningAmount, levelbefore, objecttier, (float) final_damage / (float) initial_damage);

			currentSkillExperience += experiencegain;
			evt.setDamage(final_damage);
			player.setAttribute(skill.name(), currentSkillExperience);
			levelafter = risingskills.SkillLevelHelper.getSkillLevel(skill, currentSkillExperience);

			int wear = calculateWear(levelbefore, objecttier, tooltier);
			tool.setDurability(Math.max(0,currentDurability-wear));

			// TODO replace logging progress ui by mining specific one
			Plant rockobject = net.risingworld.api.World.getPlant(evt.getGlobalID(), evt.getChunkPositionX(), evt.getChunkPositionY(), evt.getChunkPositionZ());
			LoggingProgressUI progressUI;
			if (!player.hasAttribute(ProgressUIAttibuteID))
			{
				progressUI = new LoggingProgressUI(player);
				player.addUIElement(progressUI);
				player.setAttribute(ProgressUIAttibuteID, progressUI);
			}
			else
			{
				progressUI = (LoggingProgressUI) player.getAttribute(ProgressUIAttibuteID);
			}

			short totalhealth = evt.getPlantDefinition().strength;
			short currenthealth = rockobject.getStrength();
			short finalhealth = (short) Math.max(0,  (currenthealth - final_damage));
			progressUI.show(100 - (finalhealth *100/ totalhealth));
		}
		else if (event instanceof PlayerRemoveVegetationEvent)
		{
			PlayerRemoveVegetationEvent evt = (PlayerRemoveVegetationEvent) event;
			player = evt.getPlayer();
			int currentSkillExperience = (int) player.getAttribute(skill.name());
			levelbefore = risingskills.SkillLevelHelper.getSkillLevel(skill, currentSkillExperience);
			Plants.PlantDefinition def = evt.getPlantDefinition();

			if ( isPickableMineral(def) ) {
				// picking up stones always provides very low amount of xp only
				int experiencegain = 1;

				currentSkillExperience += experiencegain;
				player.setAttribute(skill.name(), currentSkillExperience);
			}
			else {
				System.out.println(skillManager.getName() + ":  " + def.name + " isn't relevant for mining skill");
			}
			levelafter = risingskills.SkillLevelHelper.getSkillLevel(skill, currentSkillExperience);
		}
		else {
			System.out.println(skillManager.getName() + ":  Mining processMiningEvent called with unexpected event");
		}

		if ((player != null) && (levelbefore < levelafter))
		{
			processLevelUp(player, levelbefore, levelafter);
		}

		// singleplayerworkaround
		if ((player != null) && (immediateSave))
		{
			skillManager.persistPlayerData(player);
		}
	}

	/**
	 * Process level up.
	 *
	 * @param player the player
	 * @param old_level the old level
	 * @param new_level the new level
	 */
	private void processLevelUp(Player player, int old_level, int new_level)
	{
		// play level up sound
		skillManager.playSound(player, "SND_LEVEL_UP");

		updateMiningHudLevel(player, new_level);

		String i18nformat = skillManager.geti18nFormat("LEVEL_UP_MESSAGE", player);
		String i18nSkillname = skillManager.geti18nFormat(skill.name() + "_TITLE", player);
		skillManager.notifyPlayer(player, String.format(i18nformat, i18nSkillname, new_level));
	}

	/**
	 * Update mining hud level.
	 *
	 * @param player the player
	 * @param level the level
	 */
	private void updateMiningHudLevel(Player player, int level)
	{
		if (level >= 6)
		{
			MiningHud.setFeatureLevel(player, MiningHudFeatureLevel.Elevation);

		}
		else if (level >= 4)
		{
			MiningHud.setFeatureLevel(player, MiningHudFeatureLevel.Direction);
		}
		else  if (level >= 3)
		{
			MiningHud.setFeatureLevel(player, MiningHudFeatureLevel.Azimuth);
		}
	}

	/**
	 * Validate tool tier configuration.
	 */
	private void validateToolTierConfiguration()
	{
		if (config == null)
		{
			return;
		}

		Object tools_obj = config.get("tools");
		if (tools_obj == null)
		{
			System.out.println("Missing tool configuration for mining skill");
			return;
		}
		try {
			@SuppressWarnings("unchecked")
			ArrayList< HashMap<String, Object> > tool_tiers = (ArrayList< HashMap<String, Object> >) tools_obj;
			for (HashMap<String, Object> entry : tool_tiers)
			{
				String name = (String) entry.get("name");
				if (name != null)
				{
					Items.ItemDefinition item = net.risingworld.api.definitions.Definitions.getItemDefinition(name);
					if (item != null)
					{
						int pickaxetier = (int) Math.floor(item.pickingInfo.damagestone / 15.1);
						int shoveltier = (int) Math.floor(item.pickingInfo.damagesoil / 15.1);
						Tool toadd = new Tool();

						toadd.name = name;
						toadd.shoveltier = shoveltier;
						toadd.pickaxetier = pickaxetier;
						toadd.supportsMiningHud = false;
						try {
							toadd.supportsMiningHud = (boolean) entry.get("miningHUD");
						}
						catch (Exception e)
						{
							System.out.println(skillManager.getName() + " " + skillName + ": " + name + " failed to get mining hud support from config file");
						}
						tools.put(name,  toadd);
					}
					else {
						System.out.println(skillManager.getName() + " " + skillName + ": " + name + " is NOT a valid item");
					}
				}
				else {
					System.out.println(skillManager.getName() + " Mining: tool tier entry is malformed");
				}
			}

			System.out.println(skillManager.getName() + " " + skillName + ": " + tools.size() + " tools loaded");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Validate rock tier configuration.
	 */
	private void validateRockTierConfiguration()
	{
		if (config == null)
		{
			return;
		}

		Object rocks_obj = config.get("rocks");
		if (rocks_obj == null)
		{
			System.out.println(skillManager.getName() + " Mining: missing rock configuration for mining skill");
			return;
		}

		try {
			@SuppressWarnings("unchecked")
			ArrayList< HashMap<String, Object> > rocks = (ArrayList< HashMap<String, Object> >) rocks_obj;

			for (HashMap<String, Object> entry : rocks)
			{
				String name = (String) entry.get("name");
				Integer hardnessvalue = (Integer) entry.get("hardness");
				if ( (name != null) && ( hardnessvalue != null) )
				{
					rockhardness.put( name, hardnessvalue );
				}
				else {
					System.out.println(skillManager.getName() + " Mining: rock tier entry is malformed some rocks might not be configured correctly");
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Exception:" + e.getLocalizedMessage());
			System.out.println("Missing rock hardness array in yaml config");
		}

		Plants.PlantDefinition[] allrocks = net.risingworld.api.definitions.Definitions.getAllPlantDefinitions();

		for (Plants.PlantDefinition plant : allrocks)
		{
			if (plant != null)
			{
				if (plant.type == Plants.Type.Rock)
				{
					getObjectTier(plant);
				}
			}
		}

		System.out.println(skillManager.getName() + " " + skillName + ": " + rockhardness.size() + " rocks loaded");

		Object terrains_obj = config.get("terrain");
		if (terrains_obj == null)
		{
			System.out.println(skillManager.getName() + " Mining: missing terrain configuration for mining skill");
			return;
		}

		try {
			@SuppressWarnings("unchecked")
			HashMap< String, ArrayList<Object> > terrainconfig = (HashMap< String, ArrayList<Object> >) terrains_obj;

			for (String terraintype : terrainconfig.keySet())
			{
				ArrayList<Object> terrainlist =  terrainconfig.get(terraintype);

				if (terrainlist == null) {
					continue;
				}
				if (terraintype.equals("sands"))
				{
					for (int i = 0; i < terrainlist.size(); i++)
					{
						@SuppressWarnings("unchecked")
						HashMap<String, Object> entry = (HashMap<String, Object>) terrainlist.get(i);

						String terrainname = (String) entry.get("name");
						Integer hardnessvalue = (Integer) entry.get("hardness");

						if ((terrainname != null) && ( hardnessvalue != null) )
						{
							Terrain toadd = new Terrain();
							toadd.name = terrainname;
							toadd.hardness = hardnessvalue;
							toadd.type = TerrainType.Sand;
							terrains.put(terrainname,  toadd);
						}
					}
				}
				else if (terraintype.equals("solids"))
				{
					for (int i = 0; i < terrainlist.size(); i++)
					{
						@SuppressWarnings("unchecked")
						HashMap<String, Object> entry = (HashMap<String, Object>) terrainlist.get(i);

						String terrainname = (String) entry.get("name");
						Integer hardnessvalue = (Integer) entry.get("hardness");

						if ((terrainname != null) && ( hardnessvalue != null) )
						{
							Terrain toadd = new Terrain();
							toadd.name = terrainname;
							toadd.hardness = hardnessvalue;
							toadd.type = TerrainType.Solid;
							terrains.put(terrainname,  toadd);
						}
					}
				}
				else {
					System.out.println(skillManager.getName() + " Mining: unknown terrain config element: " + terraintype);
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Exception:" + e.getLocalizedMessage());
			System.out.println("Missing terrain config element in yaml config");
		}

		System.out.println(skillManager.getName() + " " + skillName + ": " + terrains.size() + " terrains loaded");
	}
}
