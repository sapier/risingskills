/*
 *
 */
package risingskills.skills;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.yaml.snakeyaml.Yaml;

import net.risingworld.api.definitions.Crafting.Category;
import net.risingworld.api.definitions.Crafting.Recipe;
import net.risingworld.api.definitions.Crafting.SubCategory;
import net.risingworld.api.events.EventMethod;
import net.risingworld.api.events.Listener;
import net.risingworld.api.events.player.PlayerCraftItemEvent;
import net.risingworld.api.events.player.PlayerKeyEvent;
import net.risingworld.api.objects.Item;
import net.risingworld.api.objects.Player;
import net.risingworld.api.utils.Key;
import risingskills.CraftingRecipeCheck;
import risingskills.CraftingSpec;
import risingskills.SkillLevelHelper;
import risingskills.SkillManager;
import risingskills.SkillManager.SkillEnum;
import risingskills.gui.CraftingProgress;


// TODO: Auto-generated Javadoc
/**
 * The Class Template.
 */
public class Carpentry extends DefaultCraftingSkill implements Listener, CraftingRecipeCheck
{
	/**
	 * Instantiates a new template.
	 *
	 * @param manager the manager
	 * @param saveimmediatelly the saveimmediatelly
	 */
	public Carpentry(SkillManager manager, boolean saveimmediatelly)
	{
		immediateSave = saveimmediatelly;
		skillManager = manager;
		ProgressUIAttibuteID = "RISING_SKILLS_CARPENTRY_PROGRESS";

		// TODO get localized name
		skillName = "Carpentry";
		skill = SkillEnum.RISING_SKILLS_CARPENTRY;
		craftingSpecs = new HashMap<String, CraftingSpec>();

		String ymlpath = manager.getPath() + "/carpentry.yml";

		try {
			Yaml yaml = new Yaml();
			String yamlraw = new String(Files.readAllBytes(Paths.get(ymlpath)));
			config = (Map<String, Object>) yaml.load(yamlraw);

			baseLearningAmount = (Integer) config.get("baseLearnAmount");
			SkillLevelHelper.setBaseXPAmount(skill, (Integer) config.get("baseXPAmount"));

			validateSpecConfiguration();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
			System.out.println(manager.getName() + " " + skillName + ": \"" + ymlpath + "\" isn't a valid yaml file => no " + skillName + " skill available");
			return;
		}

		//SkillHelper.dumpAllRecipes();
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return skillName;
	}

	/**
	 * Crafting finished.
	 *
	 * @param player the player
	 * @param recipe the recipe
	 * @param spec the spec
	 */
	@Override
	public void craftingFinished(Player player, Recipe recipe, CraftingSpec spec)
	{
		int currentSkillExperience = (int) player.getAttribute(skill.name());
		int levelbefore = risingskills.SkillLevelHelper.getSkillLevel(skill, currentSkillExperience);
		int perElementExperience = calculateExperience(baseLearningAmount, levelbefore, recipe, spec) / recipe.amount;

		for (int i = 0; i < recipe.amount; i ++) {
			if (!getCraftingSuccessfull(levelbefore, spec)) {
				continue;
			}

			Random rand = new Random();
			int variantselected = rand.nextInt(recipe.itemDef.variations + 1);
			player.getInventory().addItem(recipe.itemDef.id, recipe.itemDef.variants[variantselected].variant, 1);
			currentSkillExperience += perElementExperience;
		}

		// TODO add chance to get more material based on skill level

		player.setAttribute(skill.name(), currentSkillExperience);
		int levelafter = risingskills.SkillLevelHelper.getSkillLevel(skill, currentSkillExperience);

		// singleplayerworkaround
		if (immediateSave)
		{
			skillManager.persistPlayerData(player);
		}

		if (levelbefore < levelafter)
		{
			// play level up sound
			skillManager.playSound(player, "SND_LEVEL_UP");

			String i18nformat = skillManager.geti18nFormat("LEVEL_UP_MESSAGE", player);
			String i18nSkillname = skillManager.geti18nFormat(skill.name() + "_TITLE", player);
			skillManager.notifyPlayer(player, String.format(i18nformat, i18nSkillname, levelafter));
		}
	}

	/** The skill name. */
	private String skillName;

	/** The immediate save. */
	private boolean immediateSave;

	/** The skill manager. */
	private SkillManager skillManager;

	/** The config. */
	private Map<String, Object> config;

	/** The base learning amount. */
	private Integer baseLearningAmount;

	/** The crafting specs. */
	private HashMap<String, CraftingSpec> craftingSpecs;

	/** The skill. */
	SkillEnum skill;

	/**
	 * Validate resource tier configuration.
	 */
	private void validateSpecConfiguration()
	{
		if (config == null)
		{
			return;
		}

		Object craftingspecs_obj = config.get("craftingspecs");
		if (craftingspecs_obj == null)
		{
			System.out.println(skillManager.getName() + " " + skillName + ": missing configuration");
			return;
		}

		try {
			@SuppressWarnings("unchecked")
			ArrayList< HashMap<String, Object> > specs = (ArrayList< HashMap<String, Object> >) craftingspecs_obj;

			for (HashMap<String, Object> entry : specs)
			{
				CraftingSpec toadd = new CraftingSpec();

				toadd.name = (String) entry.get("name");
				toadd.minSkill = (Integer) entry.get("minSkillLevel");
				toadd.optimalSkill = (Integer) entry.get("optimalSkillLevel");
				toadd.craftingTime = (Double) entry.get("craftingTime");

				// optional elements
				try {
					String categoryname = (String) entry.get("category");
					toadd.category = Category.valueOf(categoryname);
				}
				catch (Exception e)
				{}

				try {
					String categoryname = (String) entry.get("subCategory");
					toadd.subCategory = SubCategory.valueOf(categoryname);
				}
				catch (Exception e)
				{}

				if ( (toadd.name != null) && ( toadd.minSkill != null) && ( toadd.optimalSkill != null) && ( toadd.craftingTime != null) )
				{
					craftingSpecs.put(toadd.name, toadd);
				}
				else {
					System.out.println(skillManager.getName() + " " + skillName + " invalid crafting spec in config ignored");
				}
			}

			System.out.println(skillManager.getName() + " " + skillName + ": " + craftingSpecs.size() + " crafting specs loaded");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Exception:" + e.getLocalizedMessage());
		}
	}

	/**
	 * Process skill event.
	 *
	 * @param event the event
	 */
	private void processSkillEvent(PlayerCraftItemEvent event)
	{
		if (config == null)
		{
			return;
		}

		// collect event information
		Player player = event.getPlayer();
		Recipe recipe = event.getRecipe();
		CraftingSpec spec = getCraftingSpec(event.getItem(), recipe);
		int amount = event.getAmount();

		// spec is null if it's not a carpentry recipe
		if (spec == null) {
			return;
		}

		// if category is configured validate it
		if ((spec.category != null) && (spec.category != recipe.category))
		{
			return;
		}

		// if sub category is configured validate it
		if ((spec.subCategory != null) && (spec.subCategory != recipe.subCategory))
		{
			return;
		}

		if (!skillManager.checkRecipeEvent(event))
		{
			return;
		}

		// the crafting will be handled by the progressUI so we need to cancel it here
		event.setCancelled(true);

		// get skill information
		int currentSkillExperience = (int) player.getAttribute(skill.name());
		int levelbefore = risingskills.SkillLevelHelper.getSkillLevel(skill, currentSkillExperience);

		if (levelbefore < spec.minSkill)
		{
			String i18nformat = skillManager.geti18nFormat("MINSKILLEVEL_REQUIRED_FOR_RECIPE", player);
			String i18nSkillname = skillManager.geti18nFormat(skill.name() + "_TITLE", player);
			String i18nrecipename = recipe.name;
			skillManager.notifyPlayer(player, String.format(i18nformat, spec.minSkill, i18nSkillname, i18nrecipename));
			return;
		}

		if (startCrafting(player, recipe))
		{
			startCraftingUI(player, skillManager, skill, recipe, spec, amount);
		}
		else {
			String i18nformat = skillManager.geti18nFormat("NOT_ENOUGH_RESOURCES_TO_CRAFT", player);
			skillManager.notifyPlayer(player, String.format(i18nformat, recipe.name));
		}
	}

	/**
	 * Gets the craftig spec.
	 *
	 * @param itemcrafted the itemcrafted
	 * @param recipeused the recipeused
	 * @return the craftig spec
	 */
	private CraftingSpec getCraftingSpec(Item itemcrafted, Recipe recipeused)
	{
		if (craftingSpecs.containsKey(recipeused.name))
		{
			CraftingSpec spec = craftingSpecs.get(recipeused.name);
			spec.variant = itemcrafted.getVariant();
			return spec;
		}
		return null;
	}

	/**
	 * Calculate experience.
	 *
	 * @param baseXP the base XP
	 * @param skilllevel the skilllevel
	 * @param recipeused the recipeused
	 * @param specused the specused
	 * @return the int
	 */
	private int calculateExperience(int baseXP, int skilllevel, Recipe recipeused, CraftingSpec specused)
	{
		int leveldiff = skilllevel - specused.optimalSkill;

		if (leveldiff <= 0)
		{
			return baseXP;
		}

		// the higher the level difference the less xp you're gonna get
		int resultingxp = Math.floorDiv(baseXP, leveldiff);

		// let's be nice and give at least 1 xp
		resultingxp = Math.max(1, resultingxp);
		return resultingxp;
	}

	/**
	 * Gets the success chance.
	 *
	 * @param skilllevel the level of carpentry skill
	 * @param spec the spec
	 * @return the success chance
	 */
	private boolean getCraftingSuccessfull(int skilllevel, CraftingSpec spec)
	{
		int successchance = 500;
		successchance += ((skilllevel - spec.minSkill) * 500) / (spec.optimalSkill - spec.minSkill);

		Random rand = new Random();
		if (rand.nextInt(1000) < successchance)
		{
			return true;
		}

		return false;
	}

	/**
	 * On player craft item event.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onPlayerCraftItemEvent(PlayerCraftItemEvent event)
	{
		processSkillEvent(event);
	}

	/**
	 * On player toggle inventory event.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onKeyInput(PlayerKeyEvent event)
	{
		if (event.getKey() == Key.Escape)
		{
			if (event.getPlayer().hasAttribute(ProgressUIAttibuteID))
			{
				CraftingProgress progressUI = (CraftingProgress) event.getPlayer().getAttribute(ProgressUIAttibuteID);

				progressUI.abortCrafting();
			}
		}
	}

	/**
	 * Check recipe event.
	 *
	 * @param event the event
	 * @return true, if successful
	 */
	@Override
	public boolean checkRecipeEvent(PlayerCraftItemEvent event) {
		// TODO Auto-generated method stub
		return true;
	}
}
