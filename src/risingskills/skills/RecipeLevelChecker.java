/*
 *
 */
package risingskills.skills;

import java.util.ArrayList;
import java.util.HashMap;

import net.risingworld.api.events.player.PlayerCraftItemEvent;
import net.risingworld.api.objects.Player;
import risingskills.SkillManager;
import risingskills.SkillManager.SkillEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class RecipeLevelChecker.
 */
public class RecipeLevelChecker {

	/**
	 * Instantiates a new recipe level checker.
	 *
	 * @param specs the specs
	 * @param skillid the skillid
	 * @param manager the manager
	 */
	public RecipeLevelChecker(ArrayList< HashMap<String, Object> > specs, SkillManager.SkillEnum skillid, SkillManager manager)
	{
		skill = skillid;
		requiredCraftLevels = new HashMap<String, Integer>();
		skillManager = manager;

		for (HashMap<String, Object> entry : specs)
		{
			String name = (String) entry.get("name");
			Integer minLevel = (Integer) entry.get("minLevel");

			requiredCraftLevels.put(name, minLevel);
		}

		System.out.println(skillManager.getName() + " RecipeLevelChecker handling " + requiredCraftLevels.size() + " craft recipes for skill " + skillid.name());
	}

	/**
	 * Check recipe event.
	 *
	 * @param event the event
	 * @return true, if successful
	 */
	protected boolean checkRecipeEvent(PlayerCraftItemEvent event)
	{
		if (!requiredCraftLevels.containsKey(event.getItem().getName()))
		{
			return true;
		}

		Player player = event.getPlayer();
		int currentSkillExperience = (int) player.getAttribute(skill.name());
		int currentLevel = risingskills.SkillLevelHelper.getSkillLevel(skill, currentSkillExperience);
		int requiredlevel = requiredCraftLevels.get(event.getItem().getName());

		if (currentLevel < requiredlevel)
		{
			String i18nFormat = skillManager.geti18nFormat("MINSKILLEVEL_REQUIRED_FOR_RECIPE", player);
			String localizedSkillName = skillManager.geti18nFormat(skill.name() + "_TITLE", player);
			String localizedRecipeName = event.getItem().getName(); // TBD
			String text = String.format(i18nFormat, requiredlevel, localizedSkillName, localizedRecipeName);
			skillManager.notifyPlayer(player, text);
			event.setCancelled(true);
			return false;
		}
		return true;
	}

	/** The required craft levels. */
	private HashMap<String, Integer> requiredCraftLevels;

	/** The skill ID. */
	private SkillEnum skill;

	/** The skill manager. */
	SkillManager skillManager;
}
