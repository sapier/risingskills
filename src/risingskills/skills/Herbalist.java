/*
 *
 */
package risingskills.skills;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;

import net.risingworld.api.definitions.Plants;
import net.risingworld.api.events.Event;
import net.risingworld.api.events.EventMethod;
import net.risingworld.api.events.Listener;
import net.risingworld.api.events.player.world.PlayerRemoveVegetationEvent;
import net.risingworld.api.objects.Item;
import net.risingworld.api.objects.Player;
import risingskills.SkillLevelHelper;
import risingskills.SkillManager;
import risingskills.SkillManager.SkillEnum;


// TODO: Auto-generated Javadoc
/**
 * The Class Template.
 */
public class Herbalist implements Listener
{

	/**
	 * Instantiates a new template.
	 *
	 * @param manager the manager
	 * @param saveimmediatelly the saveimmediatelly
	 */
	public Herbalist(SkillManager manager, boolean saveimmediatelly)
	{
		immediateSave = saveimmediatelly;
		skillManager = manager;
		tools = new HashMap<String, Tool>();
		herbs = new HashMap<String, Herb>();

		// TODO get localized name
		skillName = "herbalist";
		skill = SkillEnum.RISING_SKILLS_HERBALIST;

		String ymlpath = manager.getPath() + "/herbalist.yml";

		try {
			Yaml yaml = new Yaml();
			String yamlraw = new String(Files.readAllBytes(Paths.get(ymlpath)));
			config = (Map<String, Object>) yaml.load(yamlraw);

			baseLearningAmount = (Integer) config.get("baseLearnAmount");
			SkillLevelHelper.setBaseXPAmount(skill, (Integer) config.get("baseXPAmount"));

			validateResourceTierConfiguration();
			validateToolTierConfiguration();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
			System.out.println(manager.getName() + " " + skillName + ": \"" + ymlpath + "\" isn't a valid yaml file => no " + skillName + " skill available");
			return;
		}

		// SkillHelper.dumpAllPlants();
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return skillName;
	}

	/** The skill name. */
	private String skillName;

	/** The immediate save. */
	private boolean immediateSave;

	/** The skill manager. */
	private SkillManager skillManager;

	/** The config. */
	private Map<String, Object> config;


	/** The base learing amount. */
	private Integer baseLearningAmount;

	/** The tool tier. */
	private HashMap<String, Tool> tools;

	/** The config. */
	private Map<String, Herb> herbs;

	/** The skill. */
	SkillEnum skill;

	/**
	 * The Enum ToolType.
	 */
	private enum ToolType
	{

		/** The Herb shovel. */
		HerbShovel,

		/** The Sickle. */
		Sickle
	}

	/**
	 * The Class Herb.
	 */
	private class Herb
	{

		/** The name. */
		public String name;

		/** The required tool. */
		public ToolType requiredTool;

		/** The required skill level. */
		public int requiredSkillLevel;
	}

	/**
	 * The Class Tool.
	 */
	private class Tool
	{

		/** The name. */
		public String name;

		/** The tool tier. */
		public HashMap<ToolType, Integer> toolTier;
	}

	/**
	 * Validate tool tier configuration.
	 */
	private void validateToolTierConfiguration()
	{
		if (config == null)
		{
			return;
		}
	}

	/**
	 * Validate resource tier configuration.
	 */
	private void validateResourceTierConfiguration()
	{
		if (config == null)
		{
			return;
		}
	}

	/**
	 * Gets the tool tier.
	 *
	 * @param name the name
	 * @param type the type
	 * @return the tool tier
	 */
	private int getToolTier(String name, ToolType type)
	{
		if (tools.containsKey(name))
		{
			Tool tool = tools.get(name);

			if (tool.toolTier.containsKey(type))
			{
				return tool.toolTier.get(type);
			}
		}
		return -1;
	}

	/**
	 * Gets the resource tier.
	 *
	 * @param herb the herb
	 * @return the resource tier
	 */
	private Herb getHerbDef(Plants.PlantDefinition herb)
	{
		return new Herb();
	}

	/**
	 * Process skill event.
	 *
	 * @param event the event
	 */
	private void processSkillEvent(Event event)
	{
		if (config == null)
		{
			return;
		}

		Player player = null;
		int levelbefore = 0;
		int levelafter = 0;

		if (event instanceof PlayerRemoveVegetationEvent)
		{
			PlayerRemoveVegetationEvent evt = (PlayerRemoveVegetationEvent) event;
			// collect event information
			player = evt.getPlayer();
			Item equippedItem = player.getEquippedItem();


			Plants.PlantDefinition resource = evt.getPlantDefinition();
			Herb herb = getHerbDef(resource);
			Integer ToolTier = getToolTier(equippedItem.getName(), herb.requiredTool);

			// get skill information
			int currentSkillExperience = (int) player.getAttribute(skill.name());
			levelbefore = risingskills.SkillLevelHelper.getSkillLevel(skill, currentSkillExperience);

			// do we need to cancel the event due to lack of skill?
			// Note: it'd be better to switch the result to weed or fibers but that's not yet possible
			if (!skillSufficient(levelbefore, equippedItem, resource))
			{
				evt.setCancelled(true);
			}
			else
			{
				// process damage and experience
				int experiencegain = calculateExperience(baseLearningAmount, levelbefore, herb.requiredSkillLevel, 1);

				currentSkillExperience += experiencegain;
				player.setAttribute(skill.name(), currentSkillExperience);

				// set damage and durability changes if the tool was used at all
				if (isToolUsed(resource, equippedItem))
				{
					int currentDurability = equippedItem.getDurability();
					equippedItem.setDurability(currentDurability - calculateWear(levelbefore, herb.requiredSkillLevel, ToolTier));
				}

				levelafter = risingskills.SkillLevelHelper.getSkillLevel(skill, currentSkillExperience);
			}
		}


		if (levelbefore < levelafter)
		{
			// play level up sound
			skillManager.playSound(player, "SND_LEVEL_UP");

			String i18nformat = skillManager.geti18nFormat("LEVEL_UP_MESSAGE", player);
			String i18nSkillname = skillManager.geti18nFormat(skill.name() + "_TITLE", player);
			skillManager.notifyPlayer(player, String.format(i18nformat, i18nSkillname, levelafter));
		}

		if (player != null)
		{
			// singleplayerworkaround
			if (immediateSave)
			{
				skillManager.persistPlayerData(player);
			}
		}
	}

	/**
	 * Calculate experience.
	 *
	 * @param baseXP the base XP
	 * @param skilllevel the skilllevel
	 * @param requiredskill the requiredskill
	 * @param damagefraction the damagefraction
	 * @return the int
	 */
	private int calculateExperience(int baseXP, int skilllevel, int requiredskill, double damagefraction)
	{

		return 0;
	}

	/**
	 * Calculate wear.
	 *
	 * @param skillevel the skillevel
	 * @param requiredskill the requiredskill
	 * @param tooltier the tooltier
	 * @return the int
	 */
	private int calculateWear(int skillevel, int requiredskill, int tooltier)
	{
		float wear = 2;

		if (requiredskill > tooltier)
		{
			wear*=1.5;
		}

		float skilllevelcorrection = (float) Math.pow(0.9, (skillevel - requiredskill));

		if (skillevel > requiredskill)
		{
			wear *= skilllevelcorrection;
		}
		wear = Math.round(wear);
		return (int) wear;
	}

	/**
	 * Skill sufficient.
	 *
	 * @param skillevel the skillevel
	 * @param equippedItem the equipped item
	 * @param resource the resource
	 * @return true, if successful
	 */
	private boolean skillSufficient(int skillevel, Item equippedItem, Plants.PlantDefinition resource)
	{
		return false;
	}

	/**
	 * Checks if is tool used.
	 *
	 * @param resource the resource
	 * @param equippedItem the equipped item
	 * @return true, if is tool used
	 */
	private boolean isToolUsed(Plants.PlantDefinition resource, Item equippedItem)
	{
		return false;
	}

	/**
	 * On remove vegetation.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onRemoveVegetation(PlayerRemoveVegetationEvent event)
	{
		if ( event.getPlantDefinition().type == Plants.Type.Plant )
		{
			processSkillEvent(event);
		}
	}
}
