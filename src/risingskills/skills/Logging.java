/*
 *
 */
package risingskills.skills;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;

import net.risingworld.api.Server;
import net.risingworld.api.definitions.Items;
import net.risingworld.api.definitions.Plants;
import net.risingworld.api.definitions.Plants.PlantDefinition;
import net.risingworld.api.events.EventMethod;
import net.risingworld.api.events.Listener;
import net.risingworld.api.events.player.world.PlayerHitVegetationEvent;
import net.risingworld.api.objects.Item;
import net.risingworld.api.objects.Player;
import net.risingworld.api.objects.world.Plant;
import risingskills.SkillLevelHelper;
import risingskills.SkillManager;
import risingskills.SkillManager.SkillEnum;
import risingskills.gui.LoggingProgressUI;

// TODO: Auto-generated Javadoc
/**
 * The Class Logging.
 */
public class Logging implements Listener
{

	/** The Progress UI attibute ID. */
	private String ProgressUIAttibuteID;

	/**
	 * Instantiates a new logging.
	 *
	 * @param manager the manager
	 * @param saveimmediatelly the saveimmediatelly
	 */
	public Logging(SkillManager manager, boolean saveimmediatelly)
	{
		immediateSave = saveimmediatelly;
		skillManager = manager;
		ProgressUIAttibuteID = "RISING_SKILLS_LOGGING_PROGRESS";
		hardness = new HashMap<String, Integer>();
		toolTier = new HashMap<String, Integer>();

		String ymlpath = manager.getPath() + "/logging.yml";

		// TODO get localized name
		skillName = "Logging";
		skill = SkillEnum.RISING_SKILLS_LOGGING;

		try {
			Yaml yaml = new Yaml();
			String yamlraw = new String(Files.readAllBytes(Paths.get(ymlpath)));
			config = (Map<String, Object>) yaml.load(yamlraw);

			baseLearningAmount = (Integer) config.get("baseLearnAmount");
			SkillLevelHelper.setBaseXPAmount(skill, (Integer) config.get("baseXPAmount"));

			validateTreeTierConfiguration();
			validateToolTierConfiguration();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
			System.out.println(manager.getName() + ": \"" + ymlpath + "\" isn't a valid yaml file => no logging skill available");
			return;
		}
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return skillName;
	}

	/** The immediate save. */
	private boolean immediateSave;

	/** The skill name. */
	private String skillName;

	/** The skill manager. */
	private SkillManager skillManager;

	/** The config. */
	private Map<String, Object> config;

	/** The hardness. */
	private HashMap<String, Integer> hardness;

	/** The tool tier. */
	private HashMap<String, Integer> toolTier;

	/** The base learing amount. */
	private Integer baseLearningAmount;

	/** The last tool tier warning. */
	private float lastToolTierWarning = 0;

	/** The skill. */
	SkillEnum skill;

	/**
	 * Validate tree tier configuration.
	 */
	private void validateTreeTierConfiguration()
	{
		if (config == null)
		{
			return;
		}

		Object trees_obj = config.get("trees");
		if (trees_obj == null)
		{
			System.out.println("Missing tree configuration for logging skill");
			return;
		}

		try {
			@SuppressWarnings("unchecked")
			ArrayList< HashMap<String, Object> > trees = (ArrayList< HashMap<String, Object> >) trees_obj;

			for (HashMap<String, Object> entry : trees)
			{
				String name = (String) entry.get("name");
				Integer hardnessvalue = (Integer) entry.get("hardness");
				if ( (name != null) && ( hardnessvalue != null) )
				{
					hardness.put( name, hardnessvalue );
				}
				else {
					System.out.println(skillManager.getName() + " Logging: tree tier entry is malformed some trees might not be configured correctly");
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Exception:" + e.getLocalizedMessage());
			System.out.println("Missing tree hardness array in yaml config");
		}

		Object trunk_obj = config.get("trunks");
		if (trunk_obj == null)
		{
			System.out.println("Missing trunks configuration for logging skill");
		}
		else {
			try {
				@SuppressWarnings("unchecked")
				ArrayList< HashMap<String, Object> > trunks = (ArrayList< HashMap<String, Object> >) trunk_obj;

				for (HashMap<String, Object> entry : trunks)
				{
					String name = (String) entry.get("name");
					Integer hardnessvalue = (Integer) entry.get("hardness");
					if ( (name != null) && ( hardnessvalue != null) )
					{
						hardness.put( name, hardnessvalue );
					}
					else {
						System.out.println(skillManager.getName() + " Logging: trunk tier entry is malformed some trunks might not be configured correctly");
					}
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				System.out.println("Exception:" + e.getLocalizedMessage());
				System.out.println("Missing trunks hardness array in yaml config");
			}
		}

		System.out.println(skillManager.getName() + " " + skillName + ": " + hardness.size() + " trees loaded");

		Plants.PlantDefinition[] allplants = net.risingworld.api.definitions.Definitions.getAllPlantDefinitions();
		for (Plants.PlantDefinition plant : allplants)
		{
			if (plant != null)
			{
				if ((plant.type == Plants.Type.Tree) || (plant.type == Plants.Type.FruitTree) || (plant.type == Plants.Type.Trunk))
				{
					getTreeTier(plant);
				}
			}
		}
	}

	/**
	 * Gets the tree tier.
	 *
	 * @param plant the plant
	 * @return the tree tier
	 */
	private int getTreeTier(Plants.PlantDefinition plant)
	{
		// find hardness
		int hardnessvalue = 0;
		boolean found = false;
		for (String key : hardness.keySet())
		{
			if (plant.name.startsWith(key, 0)) {
				hardnessvalue = hardness.get(key);
				found = true;
				break;
			}
		}

		if (!found)
		{
			System.out.println(skillManager.getName() + ": missing hardness configuration for " + plant.name);
		}

		int tier = Math.floorDiv(hardnessvalue, 500);

		// saplings are always at least one tier less (but not lower than 0 of course)
		if (plant.cangrow) {
			tier = Math.max(0, tier-1);
		}
		//System.out.println(plant.name + ": hardness=" + String.valueOf(hardnessvalue) + " tier: " + String.valueOf(tier));

		return tier;
	}

	/**
	 * Gets the tool tier.
	 *
	 * @param name the name
	 * @return the tool tier
	 */
	private int getToolTier(String name)
	{
		if (toolTier.containsKey(name))
		{
			return toolTier.get(name);
		}
		return -1;
	}

	/**
	 * Validate tool tier configuration.
	 */
	private void validateToolTierConfiguration()
	{
		if (config == null)
		{
			return;
		}

		Object tools_obj = config.get("tools");
		if (tools_obj == null)
		{
			System.out.println("Missing tree configuration for logging skill");
			return;
		}
		try {
			@SuppressWarnings("unchecked")
			ArrayList< HashMap<String, Object> > tool_tiers = (ArrayList< HashMap<String, Object> >) tools_obj;
			for (HashMap<String, Object> entry : tool_tiers)
			{
				String name = (String) entry.get("name");
				if (name != null)
				{
					Items.ItemDefinition item = net.risingworld.api.definitions.Definitions.getItemDefinition(name);
					if (item != null)
					{
						int tier = (int) Math.floor(item.pickingInfo.damagetrees / 15);
						toolTier.put(name, tier);
					}
					else {
						System.out.println(skillManager.getName() + " " + skillName + ": " + name + " is NOT a valid item");
					}
				}
				else {
					System.out.println(skillManager.getName() + " Logging: tree tier entry is malformed some trees might not be configured correctly");
				}
			}

			System.out.println(skillManager.getName() + " " + skillName + ": " + tool_tiers.size() + " tools loaded");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Process logging event.
	 *
	 * @param event the event
	 */
	private void processLoggingEvent(PlayerHitVegetationEvent event)
	{
		if (config == null)
		{
			return;
		}
		Player player = event.getPlayer();
		Plants.PlantDefinition plant = event.getPlantDefinition();
		Integer TreeTier = getTreeTier(plant);

		Item equippedItem = player.getEquippedItem();
		Integer ToolTier = getToolTier(equippedItem.getName());

		int currentSkillExperience = (int) player.getAttribute(skill.name());
		int levelbefore = risingskills.SkillLevelHelper.getSkillLevel(skill, currentSkillExperience);
		short initial_damage = event.getDamage();
		int currentDurability = equippedItem.getDurability();

		Plant treeobject = net.risingworld.api.World.getPlant(event.getGlobalID(), event.getChunkPositionX(), event.getChunkPositionY(), event.getChunkPositionZ());

		// process damage and experience
		short final_damage = calculateDamage(player, initial_damage, levelbefore, TreeTier, ToolTier, currentDurability);
		int experiencegain = calculateExperience(baseLearningAmount, levelbefore, TreeTier, (float) final_damage/ (float) initial_damage);
		currentSkillExperience += experiencegain;
		event.setDamage(final_damage);
		player.setAttribute(skill.name(), currentSkillExperience);
		int levelafter = risingskills.SkillLevelHelper.getSkillLevel(skill, currentSkillExperience);

		// calculate durability changes
		System.out.println("Current tool durability: " + currentDurability);
		equippedItem.setDurability(currentDurability - calculateWear(levelbefore, TreeTier, ToolTier));

		if (levelbefore < levelafter)
		{
			// play level up sound
			skillManager.playSound(player, "SND_LEVEL_UP");

			String i18nformat = skillManager.geti18nFormat("LEVEL_UP_MESSAGE", player);
			String i18nSkillname = skillManager.geti18nFormat(skill.name() + "_TITLE", player);
			skillManager.notifyPlayer(player, String.format(i18nformat, i18nSkillname, levelafter));
		}

		// singleplayerworkaround
		if (immediateSave)
		{
			skillManager.persistPlayerData(player);
		}

		LoggingProgressUI progressUI;
		if (!player.hasAttribute(ProgressUIAttibuteID))
		{
			progressUI = new LoggingProgressUI(player);
			player.addUIElement(progressUI);
			player.setAttribute(ProgressUIAttibuteID, progressUI);
		}
		else
		{
			progressUI = (LoggingProgressUI) player.getAttribute(ProgressUIAttibuteID);
		}

		short totalHealth = event.getPlantDefinition().strength;
		short currentHealth = (short) Math.max(0,  (treeobject.getStrength() - final_damage));
		progressUI.show(100 - (currentHealth *100/ totalHealth));
	}

	/**
	 * Calculate experience.
	 *
	 * @param baseXP the base XP gained for this skill
	 * @param skilllevel the current level of the skill
	 * @param planttier the tier the plant is at
	 * @param damagefraction the relative amount of damage based on skill and tool
	 * @return the int
	 */
	private int calculateExperience(int baseXP, int skilllevel, int planttier, double damagefraction)
	{
		int leveldiff = skilllevel - planttier;

		// as we're using a division to adjust the xp we have to increase it by one to get
		// base xp on no level difference
		leveldiff += 1;

		// full xp if the player is not yet at the given level
		// ... well not exactly it's gonna be reduced by the damagefragtion later
		if (leveldiff < 1)
		{
			leveldiff = 1;
		}

		// the higher the level difference the less xp you're gonna get
		int resultingxp = Math.floorDiv(baseXP, leveldiff);

		// based on tool and player level the damage is scaled this may have a negative effect on experience too
		resultingxp *= Math.min(damagefraction, 1);

		// let's be nice and give at least 1 xp
		resultingxp = Math.max(1, resultingxp);
		//System.out.println("calculateExperience: base="  + baseXP + " calculated=" + resultingxp + " skillevel=" + skilllevel + " planttier=" + planttier + " damagefraction=" + damagefraction);
		return resultingxp;
	}

	/**
	 * Calculate damage.
	 *
	 * @param player the player
	 * @param basedamage the basedamage
	 * @param skillevel the skillevel
	 * @param planttier the planttier
	 * @param tooltier the tier the wielded tool is at
	 * @param durability the toolhealth
	 * @return the double
	 */
	private short calculateDamage(Player player, short basedamage, int skillevel, int planttier, int tooltier, int durability)
	{
		if (tooltier < 0)
		{
			return 0;
		}

		if (durability <= 0)
		{
			String i18nformat = skillManager.geti18nFormat("TOOL_BROKEN_WARNING_GENERIC", player);
			skillManager.notifyPlayer(player,String.format(i18nformat));
			return 0;
		}

		// reduce damage drastically if tool isn't suitable for tree
		if (planttier > tooltier)
		{
			//System.out.println("calculateDamage: tool not sufficient reduce damage drastically " + tooltier + " < " + planttier);
			basedamage *= 0.5;
			float now = Server.getRunningTime();

			if (now > lastToolTierWarning + 10)
			{
				String i18nformat = skillManager.geti18nFormat("TOOL_TIER_INSUFFICIENT", player);
				skillManager.notifyPlayer(player,String.format(i18nformat));
			}
			lastToolTierWarning = now;
		}

		// shortcut for simple tools ... and to avoid division by 0 ;-)
		if (tooltier == 0) {
			//System.out.println("calculateDamage: tier 0 base="  + basedamage);
			return basedamage;
		}

		int resulting_damage = basedamage + basedamage * (skillevel/(tooltier*3));

		// slow down damage increase
		if (skillevel > 6* tooltier)
		{
			resulting_damage = basedamage + (int) Math.floor((2 * basedamage) + ((skillevel - 6) * 0.1* basedamage));
		}

		System.out.println("calculateDamage: base="  + basedamage + " calculated=" + resulting_damage + " skillevel=" + skillevel + " tooltier=" + tooltier);

		return (short) resulting_damage;
	}

	/**
	 * Calculate wear.
	 *
	 * @param skillevel the skillevel
	 * @param planttier the planttier
	 * @param tooltier the tooltier
	 * @return the int
	 */
	private int calculateWear(int skillevel, int planttier, int tooltier)
	{
		float wear = 2;

		if (planttier > tooltier)
		{
			wear*=1.5;
		}

		float skilllevelcorrection = (float) Math.pow(0.9, (skillevel - planttier));

		if (skillevel > planttier)
		{
			wear *= skilllevelcorrection;
		}
		wear = Math.round(wear);
		System.out.println("Wear: skillcorrection=" + skilllevelcorrection + " planttier=" + planttier + " tooltier=" + tooltier + " wear=" + wear);
		return (int) wear;
	}

	/**
	 * On vegetation hit.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onVegetationHit(PlayerHitVegetationEvent event)
	{
		PlantDefinition hitplant = event.getPlantDefinition();
		switch (hitplant.type)
		{
		case API:
			break;

		case Crop:
			break;

		case Plant:
			break;

		case Rock:
			break;

		case FruitTree:
		case Tree:
		case Trunk:
			processLoggingEvent(event);
			break;

		case Undefined:
			break;

		case Underwater:
			break;

		case Water:
			break;

		default:
			break;
		}
	}
}
