/*
 *
 */
package risingskills.skills;

import net.risingworld.api.definitions.Crafting;
import net.risingworld.api.definitions.Crafting.Recipe;
import net.risingworld.api.definitions.Crafting.RecipeType;
import net.risingworld.api.definitions.Definitions;
import net.risingworld.api.definitions.Objects.ObjectDefinition;
import net.risingworld.api.objects.Inventory;
import net.risingworld.api.objects.Item;
import net.risingworld.api.objects.Player;
import risingskills.CraftingSpec;
import risingskills.SkillManager;
import risingskills.SkillManager.SkillEnum;
import risingskills.gui.CraftingProgress;


// TODO: Auto-generated Javadoc
interface CraftingSkill {
	public boolean startCrafting(Player player, Recipe recipe);
	public void craftingFinished(Player player, Recipe recipe, CraftingSpec spec);
	public void startCraftingUI(Player player, SkillManager manager, SkillEnum skill, Recipe recipe, CraftingSpec spec, int amount);
}

/**
 * The Class DefaultSkill.
 */
public class DefaultCraftingSkill implements CraftingSkill
{

	/** The Progress UI attibute ID. */
	public String ProgressUIAttibuteID;

	/**
	 * Instantiates a new default crafting skill.
	 */
	public DefaultCraftingSkill()
	{
		ProgressUIAttibuteID = SkillEnum.RISING_SKILLS_CRAFTING_DEFAULT.name() + "_PROGRESS";
	}

	/**
	 * Start crafting.
	 *
	 * @param player the player
	 * @param recipe the recipe
	 * @return true, if successful
	 */
	@Override
	public boolean startCrafting(Player player, Recipe recipe) {
		if (! hasAllIngredients(player, recipe.ingredients))
		{
			return false;
		}

		takeAllIngredients( player, recipe.ingredients);

		return true;
	}

	/**
	 * Crafting finished.
	 *
	 * @param player the player
	 * @param recipe the recipe
	 * @param spec the spec
	 */
	@Override
	public void craftingFinished(Player player, Recipe recipe, CraftingSpec spec)
	{
		boolean isConstructionRecipe = (recipe.type == RecipeType.Construction);
		boolean isObjectRecipe = (recipe.type == RecipeType.Object);
		boolean isClothingRecipe = (recipe.type == RecipeType.Clothing);
		boolean isBlockRecipe = (recipe.type == RecipeType.Block);

		if (isConstructionRecipe) {
		}
		else if (isObjectRecipe) {
			ObjectDefinition objdef = Definitions.getObjectDefinition(recipe.name);
			player.getInventory().addObjectItem(objdef.id, spec.variant, recipe.amount);
		}
		else if (isClothingRecipe) {
		}
		else if (isBlockRecipe) {
		}
		else {
			Item added = player.getInventory().addItem(recipe.itemDef.id, spec.variant, recipe.amount);

			// fix the durability of the item
			added.setDurability(recipe.itemDef.durability);
		}
	}

	/**
	 * Checks for all ingredients.
	 *
	 * @param player the player
	 * @param ingredients the ingredients
	 * @return true, if successful
	 */
	private boolean hasAllIngredients(Player player, Crafting.Recipe.Ingredient[] ingredients)
	{
		Inventory inventory = player.getInventory();

		Item itemlist[] = inventory.getAllItems();

		for ( Crafting.Recipe.Ingredient ingredient : ingredients )
		{
			int amountrequired = ingredient.count;

			for (Item elem : itemlist)
			{
				if (elem == null) {
					continue;
				}
				// itemdef may be null if the ingredient doesn't require a specific item
				if (ingredient.itemDef != null)
				{
					if (elem.getTypeID() == ingredient.itemDef.id)
					{
						amountrequired -= Math.min(elem.getStack(), amountrequired);
					}
				}
				else {
					// if it's enough for the recipe to have a random item from a group
					if (ingredient.group != null)
					{
						if (elem.getDefinition().group == ingredient.group)
						{
							amountrequired -= Math.min(elem.getStack(), amountrequired);
						}
					}
				}

				if (amountrequired <= 0)
				{
					break;
				}
			}

			if (amountrequired > 0)
			{
				return false;
			}
		}

		return true;
	}

	/**
	 * Take all ingredients.
	 *
	 * @param player the player
	 * @param ingredients the ingredients
	 */
	private void takeAllIngredients(Player player, Crafting.Recipe.Ingredient[] ingredients)
	{
		Inventory inventory = player.getInventory();

		for ( Crafting.Recipe.Ingredient ingredient : ingredients )
		{
			int amountrequired = ingredient.count;

			int maxloop = 1000;
			while ((amountrequired > 0) && (maxloop > 0))
			{
				if (ingredient.itemDef != null)
				{
					int slots[] = inventory.findAllItems(ingredient.itemDef.id, -1, Inventory.SlotType.Inventory);

					for (int slot : slots)
					{
						Item inslot = inventory.getItem(slot, Inventory.SlotType.Inventory);
						if (inslot != null)
						{
							int canbetaken = Math.min(amountrequired, inslot.getStack());
							inventory.removeItem(slot, Inventory.SlotType.Inventory, canbetaken);
							amountrequired -= canbetaken;
						}
					}
				}
				if (ingredient.itemDef != null)
				{
					int slots[] = inventory.findAllItems(ingredient.itemDef.id, -1, Inventory.SlotType.Quickslot);

					for (int slot : slots)
					{
						Item inslot = inventory.getItem(slot, Inventory.SlotType.Inventory);
						if (inslot != null)
						{
							int canbetaken = Math.min(amountrequired, inslot.getStack());
							inventory.removeItem(slot, Inventory.SlotType.Inventory, canbetaken);
							amountrequired -= canbetaken;
						}
					}
				}
				if (ingredient.group != null)
				{
					int slot = inventory.findItemByGroup(ingredient.group, Inventory.SlotType.Inventory);
					Item inslot = inventory.getItem(slot, Inventory.SlotType.Inventory);
					if (inslot != null)
					{
						int canbetaken = Math.min(amountrequired, inslot.getStack());
						inventory.removeItem(slot, Inventory.SlotType.Inventory, canbetaken);
						amountrequired -= canbetaken;
					}
				}
				if (ingredient.group != null)
				{
					int slot = inventory.findItemByGroup(ingredient.group, Inventory.SlotType.Quickslot);
					Item inslot = inventory.getItem(slot, Inventory.SlotType.Inventory);
					if (inslot != null)
					{
						int canbetaken = Math.min(amountrequired, inslot.getStack());
						inventory.removeItem(slot, Inventory.SlotType.Inventory, canbetaken);
						amountrequired -= canbetaken;
					}
				}
			}
			if (maxloop <= 0)
			{
				System.out.println("takeAllIngredients wasn't able to fetch all required ingredients that's not supposed to happen!");
			}
		}
	}

	/**
	 * Start crafting UI.
	 *
	 * @param player the player
	 * @param manager the manager
	 * @param skill the skill
	 * @param recipe the recipe
	 * @param spec the spec
	 * @param amount the amount
	 * @return true, if successful
	 */
	@Override
	public void startCraftingUI(Player player, SkillManager manager, SkillEnum skill, Recipe recipe, CraftingSpec spec, int amount)
	{
		CraftingProgress progressUI;
		if (!player.hasAttribute(ProgressUIAttibuteID))
		{
			progressUI = new CraftingProgress(player, manager, skill);
			player.addUIElement(progressUI);
			player.setAttribute(ProgressUIAttibuteID, progressUI);
		}
		else
		{
			progressUI = (CraftingProgress) player.getAttribute(ProgressUIAttibuteID);
		}

		if (!progressUI.isActive())
		{
			progressUI.startCrafting(recipe, spec, amount);
		}
	}
}
