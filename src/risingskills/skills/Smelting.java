/*
 *
 */
package risingskills.skills;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;

import net.risingworld.api.Server;
import net.risingworld.api.World;
import net.risingworld.api.events.EventMethod;
import net.risingworld.api.events.Listener;
import net.risingworld.api.events.player.PlayerCraftItemEvent;
import net.risingworld.api.objects.MetaObject;
import net.risingworld.api.objects.Player;
import net.risingworld.api.objects.WorldItem;
import net.risingworld.api.utils.Utils;
import net.risingworld.api.utils.Vector3f;
import risingskills.CraftingRecipeCheck;
import risingskills.SkillLevelHelper;
import risingskills.SkillManager;
import risingskills.SkillManager.SkillEnum;
import risingskills.hacks.FurnaceIngotTakenEvent;
import risingskills.hacks.FurnaceSmeltEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class Template.
 */
public class Smelting implements Listener, CraftingRecipeCheck
{

	/**
	 * Instantiates a new template.
	 *
	 * @param manager the manager
	 * @param saveimmediatelly the saveimmediatelly
	 */
	public Smelting(SkillManager manager, boolean saveimmediatelly)
	{
		immediateSave = saveimmediatelly;
		skillManager = manager;

		// TODO get localized name
		skillName = "Smelting";
		skill = SkillEnum.RISING_SKILLS_SMELTING;

		String ymlpath = manager.getPath() + "/smelting.yml";

		try {
			Yaml yaml = new Yaml();
			String yamlraw = new String(Files.readAllBytes(Paths.get(ymlpath)));
			config = (Map<String, Object>) yaml.load(yamlraw);

			if (config == null)
			{
				System.out.println(manager.getName() + " " + skillName + ": \"" + ymlpath + "\" isn't a valid yaml file => no " + skillName + " skill available");
				return;
			}

			baseLearningAmount = (Integer) config.get("baseLearnAmount");
			minNotificationDistance = (Integer) config.get("minNotificationDistance");
			perLevelDistanceIncrease = (Integer) config.get("perLevelDistanceIncrease");
			detailedNotificationLevel = (Integer) config.get("detailedNotificationLevel");

			SkillLevelHelper.setBaseXPAmount(skill, (Integer) config.get("baseXPAmount"));

			Object recipelevel_obj = config.get("recipeRequirements");
			if (recipelevel_obj != null)
			{
				recipeCheck = new RecipeLevelChecker((ArrayList< HashMap<String, Object> >) recipelevel_obj, skill, skillManager);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
			System.out.println(manager.getName() + " " + skillName + ": \"" + ymlpath + "\" isn't a valid yaml file => no " + skillName + " skill available");
			return;
		}
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return skillName;
	}

	/** The skill name. */
	private String skillName;

	/** The immediate save. */
	private boolean immediateSave;

	/** The skill manager. */
	private SkillManager skillManager;

	/** The config. */
	private Map<String, Object> config;

	/** The base learing amount. */
	private Integer baseLearningAmount;

	/** The skill. */
	SkillEnum skill;

	/** The min notification distance. */
	private int minNotificationDistance;

	/** The per level distance increase. */
	private int perLevelDistanceIncrease;

	/** The detailed notificatio level. */
	private int detailedNotificationLevel;

	/** The recipe check. */
	private RecipeLevelChecker recipeCheck;

	/**
	 * Process crafting complete.
	 *
	 * @param event the event
	 */
	private void processCraftingComplete(FurnaceIngotTakenEvent event)
	{
		Player player = Server.getPlayerByName(event.getPlayerTakeItemName());

		if (!event.getPlayerPutItemName().equals(event.getPlayerTakeItemName()))
		{
			System.out.println(skillManager.getName() + " " + skillName + ": player: " + event.getPlayerTakeItemName() + " didn't place this ingot but " + event.getPlayerPutItemName() + " so not granting xp");
			return;
		}
		WorldItem item = World.getItem(event.getSmeltedItemID());

		System.out.println(skillManager.getName() + " " + skillName + ": player: " + event.getPlayerTakeItemName() + " finished smelting " + item.getDefinition().name);

		// get skill information
		int currentSkillExperience = (int) player.getAttribute(skill.name());
		int levelbefore = risingskills.SkillLevelHelper.getSkillLevel(skill, currentSkillExperience);

		// process damage and experience
		int experiencegain = calculateExperience(baseLearningAmount, levelbefore, 0);

		currentSkillExperience += experiencegain;
		player.setAttribute(skill.name(), currentSkillExperience);
		int levelafter = risingskills.SkillLevelHelper.getSkillLevel(skill, currentSkillExperience);
		if (levelbefore < levelafter)
		{
			// play level up sound
			skillManager.playSound(player, "SND_LEVEL_UP");

			String i18nformat = skillManager.geti18nFormat("LEVEL_UP_MESSAGE", player);
			String i18nSkillname = skillManager.geti18nFormat(skill.name() + "_TITLE", player);
			skillManager.notifyPlayer(player, String.format(i18nformat, i18nSkillname, levelafter));
		}

		// singleplayerworkaround
		if ((player != null) && (immediateSave))
		{
			skillManager.persistPlayerData(player);
		}
	}

	/**
	 * Notify player.
	 * If the players skill level is high enough a message will be sent about the smelting being finished
	 *
	 * @param event the event
	 */
	private void notifyPlayer(FurnaceSmeltEvent event)
	{
		Player player = Server.getPlayerByName(event.getPlayerPutItemName());

		if (player == null)
		{
			//System.out.println(skillManager.getName() + " " + skillName + ": notifyPlayer player="  + craftresult.playerName + " not found" );
			return;
		}

		MetaObject furnace = World.getMetaObject(event.getFurnaceMetaObjectID());

		if (furnace == null)
		{
			//System.out.println(skillManager.getName() + " " + skillName + ": notifyPlayer furnace="  + craftresult.furnaceID + " not found" );
			return;
		}

		Vector3f ppos = player.getPosition();
		Vector3f fpos = furnace.getWorldPosition();

		float distance = Utils.MathUtils.distance(ppos.x, ppos.y, ppos.z, fpos.x, fpos.y, fpos.z);
		//System.out.println(skillManager.getName() + " " + skillName + ": notifyPlayer distance="  + distance );

		int currentSkillExperience = (int) player.getAttribute(skill.name());
		int level = risingskills.SkillLevelHelper.getSkillLevel(skill, currentSkillExperience);

		if (distance > (minNotificationDistance + (level * perLevelDistanceIncrease)))
		{
			return;
		}

		if (level > detailedNotificationLevel)
		{
			String i18nformat = skillManager.geti18nFormat("SMELTING_COMPLETED", player);
			skillManager.notifyPlayer(player, String.format(i18nformat));
		}
		else if (level > (detailedNotificationLevel * 2))
		{
			WorldItem item = World.getItem(event.getSmeltedItemID());
			String i18nformat = skillManager.geti18nFormat("SMELTING_COMPLETED_NAMED", player);
			skillManager.notifyPlayer(player, String.format(i18nformat, event.getPlayerPutItemName(), item.getDefinition().name));
		}
	}

	/**
	 * Calculate experience.
	 *
	 * @param baseXP the base XP
	 * @param skilllevel the skilllevel
	 * @param resourcetier the resourcetier
	 * @return the int
	 */
	private int calculateExperience(int baseXP, int skilllevel, int resourcetier)
	{
		return baseXP;
	}

	/**
	 * On furnace smelt event.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onFurnaceSmeltEvent(FurnaceSmeltEvent event)
	{
		notifyPlayer(event);
	}

	/**
	 * On furnace ingot taken event.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onFurnaceIngotTakenEvent(FurnaceIngotTakenEvent event)
	{
		if (recipeCheck != null)
		{
			processCraftingComplete(event);
		}
	}

	/**
	 * Check recipe event.
	 *
	 * @param event the event
	 * @return true, if successful
	 */
	@Override
	public boolean checkRecipeEvent(PlayerCraftItemEvent event) {
		// TODO Auto-generated method stub
		return recipeCheck.checkRecipeEvent(event);
	}
}
