/*
 *
 */
package risingskills;

import net.risingworld.api.definitions.Crafting.Category;
import net.risingworld.api.definitions.Crafting.SubCategory;

// TODO: Auto-generated Javadoc
/**
 * The Class CraftingSpec.
 */
public class CraftingSpec {

	/**
	 * Instantiates a new crafting spec.
	 */
	public CraftingSpec()
	{
		minSkill = 999;
		optimalSkill = 999;
		name = "uninitialized";
		craftingTime = 0d;
		category = null;
		subCategory = null;
	}

	/** The name. */
	public String name;

	/** The crafting time. */
	public Double craftingTime;

	/** The min skill. */
	public Integer minSkill;

	/** The optimal skill. */
	public Integer optimalSkill;

	/** The category. */
	public Category category;

	/** The Sub category. */
	public SubCategory subCategory;

	/**  variant *. */
	public int variant;
}
