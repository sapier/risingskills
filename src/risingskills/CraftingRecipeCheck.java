/*
 * 
 */
package risingskills;

import net.risingworld.api.events.player.PlayerCraftItemEvent;

// TODO: Auto-generated Javadoc
/**
 * The Interface CraftingRecipeCheck.
 */
//TODO: Auto-generated Javadoc
public interface CraftingRecipeCheck {
	
	/**
	 * Check recipe event.
	 *
	 * @param event the event
	 * @return true, if successful
	 */
	public boolean checkRecipeEvent(PlayerCraftItemEvent event);
}
