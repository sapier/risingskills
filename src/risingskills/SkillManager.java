/*
 *
 */
package risingskills;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;

import net.risingworld.api.Plugin;
import net.risingworld.api.Server;
import net.risingworld.api.Timer;
import net.risingworld.api.database.Database;
import net.risingworld.api.definitions.Crafting.Recipe;
import net.risingworld.api.definitions.Crafting.RecipeType;
import net.risingworld.api.events.EventMethod;
import net.risingworld.api.events.Listener;
import net.risingworld.api.events.player.PlayerCommandEvent;
import net.risingworld.api.events.player.PlayerConnectEvent;
import net.risingworld.api.events.player.PlayerCraftItemEvent;
import net.risingworld.api.events.player.PlayerDisconnectEvent;
import net.risingworld.api.events.player.PlayerGameObjectHitEvent;
import net.risingworld.api.events.player.PlayerHitItemEvent;
import net.risingworld.api.events.player.PlayerKeyEvent;
import net.risingworld.api.events.player.PlayerPickupItemEvent;
import net.risingworld.api.events.player.PlayerSpawnEvent;
import net.risingworld.api.events.player.inventory.PlayerInventoryItemEditEvent;
import net.risingworld.api.events.player.inventory.PlayerInventoryToStorageEvent;
import net.risingworld.api.events.player.inventory.PlayerStorageMoveItemEvent;
import net.risingworld.api.events.player.world.PlayerChangeObjectInfoEvent;
import net.risingworld.api.events.player.world.PlayerChangeObjectStatusEvent;
import net.risingworld.api.events.player.world.PlayerHitObjectEvent;
import net.risingworld.api.events.player.world.PlayerHitTerrainEvent;
import net.risingworld.api.events.player.world.PlayerHitVegetationEvent;
import net.risingworld.api.events.player.world.PlayerRemoveConstructionEvent;
import net.risingworld.api.events.player.world.PlayerRemoveObjectEvent;
import net.risingworld.api.events.player.world.PlayerRemoveVegetationEvent;
import net.risingworld.api.objects.Item;
import net.risingworld.api.objects.Player;
import net.risingworld.api.utils.Key;
import risingskills.gui.NotificationBar;
import risingskills.gui.SkillUI;
import risingskills.gui.ToolDurability;
import risingskills.hacks.HackManager;
import risingskills.skills.Carpentry;
import risingskills.skills.DefaultCraftingSkill;
import risingskills.skills.Herbalist;
import risingskills.skills.Logging;
import risingskills.skills.Mining;
import risingskills.skills.Smelting;

// TODO: Auto-generated Javadoc
/**
 * The Class SkillManager.
 */
public class SkillManager extends Plugin implements Listener, Runnable
{

	/** The save timer. */
	private Timer saveTimer;

	/** The immediate save. */
	private boolean immediateSave;

	/** The Plugin version. */
	private String PluginVersion = "0.0.4";

	/** The skilldatabase. */
	private Database skilldatabase;

	/** The Skill U I attr key. */
	private String SkillUI_Attr_Key = "RISING_SKILLS_UI";

	/** The skills. */
	private HashMap<SkillEnum, Object> skills;

	/** The i18n data. */
	private HashMap<String, HashMap< String, String> > i18nData;

	/**
	 * The Enum SkillEnum.
	 */
	public enum SkillEnum {

		/** The rising skills logging. */
		RISING_SKILLS_LOGGING,

		/** The rising skills carpentry. */
		RISING_SKILLS_CARPENTRY,

		/** The rising skills herbalist. */
		RISING_SKILLS_HERBALIST,

		/** The rising skills mining. */
		RISING_SKILLS_MINING,

		/** The rising skills smelting. */
		RISING_SKILLS_SMELTING,

		/** The rising skills smithing. */
		RISING_SKILLS_SMITHING,

		/** The rising skills crafting default. */
		RISING_SKILLS_CRAFTING_DEFAULT,
	}

	/**
	 * Persist player data.
	 *
	 * @param player the player
	 */
	public void persistPlayerData(Player player)
	{
		System.out.println(this.getName() + ":  persistPlayerData player: " + player.getName());
		String playerid = player.getUID();
		for (SkillEnum skill : SkillEnum.values())
		{
			String skillid = skill.name();
			int experience = (int) player.getAttribute(skillid);
			try {
				String query = "UPDATE `SkillTable` SET `experience`=" + experience + " WHERE `playeruid`='" + playerid + "' AND `skillid`='" + skillid + "';";
				//System.out.println("persistSkillData: " + query);
				skilldatabase.execute(query);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	/**
	 * Gets the skill.
	 *
	 * @param skill the skill
	 * @return the skill
	 */
	public Object getSkill(SkillEnum skill)
	{
		return skills.get(skill);
	}

	/**
	 * Run.
	 */
	@Override
	public void run() {
		persistSkillData();
	}

	/**
	 * On enable.
	 */
	@Override
	public void onEnable()
	{
		System.out.println(this.getName() + " version " + PluginVersion + " initializing");

		// open database
		initializeDatabase();

		// single player mode is missing a hook to store data on shutdown so we need to do it immediatelly
		if (Server.getType() == Server.Type.Singleplayer)
		{
			System.out.println(this.getName() + " Singleplayermode detected saving skill data immediately");
			immediateSave = true;
		}
		else {
			// start cyclic skill persistency timer
			saveTimer = new Timer(60,60,-1,this);
			saveTimer.start();

			immediateSave = false;
		}

		loadi18n();

		// load sounds
		soundHelper = new SoundHelper(this);

		// initialize hacks
		hackManager = new HackManager(this);

		skills = new HashMap<SkillEnum, Object>();

		Logging logging = new Logging(this, immediateSave);
		skills.put(SkillEnum.RISING_SKILLS_LOGGING, logging);
		registerEventListener(logging);
		hackManager.registerEventListener(logging);

		Mining mining = new Mining(this, immediateSave);
		skills.put(SkillEnum.RISING_SKILLS_MINING, mining);
		registerEventListener(mining);
		hackManager.registerEventListener(mining);

		Herbalist herbalist = new Herbalist(this, immediateSave);
		skills.put(SkillEnum.RISING_SKILLS_HERBALIST, herbalist);
		registerEventListener(herbalist);
		hackManager.registerEventListener(herbalist);

		Carpentry carpentry = new Carpentry(this, immediateSave);
		skills.put(SkillEnum.RISING_SKILLS_CARPENTRY, carpentry);
		registerEventListener(carpentry);
		hackManager.registerEventListener(carpentry);

		Smelting smelting = new Smelting(this, immediateSave);
		skills.put(SkillEnum.RISING_SKILLS_SMELTING, smelting);
		registerEventListener(smelting);
		hackManager.registerEventListener(smelting);

		if (!Files.exists(Paths.get(getPath() + "/disable_crafting_delay")))
		{
			defaultcraftingskill = new DefaultCraftingSkill();
			skills.put(SkillEnum.RISING_SKILLS_CRAFTING_DEFAULT, defaultcraftingskill);
		}

		// register event listener
		registerEventListener(this);
		hackManager.registerEventListener(this);
	}

	/**
	 * On disable.
	 */
	@Override
	public void onDisable()
	{
		if(skilldatabase != null) {
			persistSkillData();
			skilldatabase.close();
		}
		else {
			System.out.println("Rising Skills Plugin strange we didn't have a valid skill database");
		}
		System.out.println("Rising Skills Plugin disabled");
	}

	/**
	 * Gets the i 18 n format.
	 *
	 * @param key the key
	 * @param player the player
	 * @return the i 18 n format
	 */
	public String geti18nFormat(String key, Player player)
	{
		if (i18nData == null)
		{
			return "";
		}

		String language = player.getLanguage();
		if (! i18nData.containsKey(language))
		{
			language = "default";
		}

		HashMap< String, String> languagedata = i18nData.get(language);
		if ( ( languagedata != null) && (languagedata.containsKey(key)) )
		{
			return languagedata.get(key);
		}

		return "";
	}

	/**
	 * Notify player.
	 *
	 * @param player the player
	 * @param message the message
	 */
	public void notifyPlayer(Player player, String message)
	{
		NotificationBar infobar = (NotificationBar) player.getAttribute(NotificationBar.attrID);

		if (infobar == null)
		{
			infobar = new NotificationBar(player);
		}

		infobar.showMessage(message);
	}

	/**
	 * Check recipe event.
	 *
	 * @param event the event
	 * @return true, if successful
	 */
	public boolean checkRecipeEvent(PlayerCraftItemEvent event)
	{
		boolean retval = true;
		for (SkillEnum entry : SkillEnum.values())
		{
			Object skill = skills.get(entry);
			if (skill instanceof CraftingRecipeCheck)
			{
				retval &= ((CraftingRecipeCheck) skill).checkRecipeEvent(event);
			}
		}

		return retval;
	}

	/**
	 * Play sound.
	 *
	 * @param player the player
	 * @param soundname the soundname
	 */
	public void playSound(Player player, String soundname)
	{
		soundHelper.playSound(player, soundname);
	}

	/**
	 * Loadi 18 n.
	 */
	private void loadi18n()
	{
		String i18npath = getPath() + "/i18n.yml";
		i18nData = new HashMap<String, HashMap< String, String> >();

		try {
			Yaml yaml = new Yaml();
			String yamlraw = new String(Files.readAllBytes(Paths.get(i18npath)));
			@SuppressWarnings("unchecked")
			Map<String, Object> i18ndata = (Map<String, Object>) yaml.load(yamlraw);

			@SuppressWarnings("unchecked")
			ArrayList< String > languages = (ArrayList< String >) i18ndata.get("languages");

			for (String language : languages)
			{
				HashMap< String, String> languagedata = new HashMap< String, String>();
				i18nData.put(language, languagedata);
			}

			@SuppressWarnings("unchecked")
			ArrayList < Object > keys = (ArrayList <Object>) i18ndata.get("keys");

			for (int i = 0; i < keys.size(); i++)
			{
				@SuppressWarnings("unchecked")
				HashMap<String, String> formats = (HashMap<String, String>) keys.get(i);

				String keyname = formats.get("name");

				for (String language : languages)
				{
					if (formats.containsKey(language))
					{
						i18nData.get(language).put(keyname, formats.get(language));
					}
					else {
						System.out.println(this.getName() + ": missing " + language + " translation for " + keyname);
					}
				}

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
			System.out.println(getName() + ": \"" + i18npath + "\" isn't a valid yaml file for internationalization => no text available");
			return;
		}

		System.out.println(getName() + ": loaded " + i18nData.size() + " languages");
	}

	/**
	 * Persist skill data.
	 */
	private void persistSkillData()
	{
		for(Player player : Server.getAllPlayers()) {
			persistPlayerData(player);
		}
	}

	/**
	 * Initialize database.
	 */
	private void initializeDatabase()
	{
		skilldatabase = getSQLiteConnection(getPath() + "/skills.db");

		String query = "CREATE TABLE IF NOT EXISTS `SkillTable` (`playeruid` VARCHAR(255) NOT NULL, `skillid` VARCHAR(255) NOT NULL, `experience` BIGINT, PRIMARY KEY(playeruid, skillid));";
		// Create new table if it does not yet exist
		skilldatabase.execute(query);
	}

	/**
	 * Gets the DB attribute data.
	 *
	 * @param playerid the playerid
	 * @param skill the skill
	 * @return the DB attribute data
	 */
	private int getDBAttributeData(String playerid, SkillEnum skill)
	{
		try(ResultSet result = skilldatabase.executeQuery("SELECT * FROM `SkillTable` WHERE `playeruid` = '" + playerid + "' AND `skillid` = '" + skill.name() + "';"))
		{
			while(result.next()) {
				//Do something with the data
				int experience = result.getInt("experience");
				return experience;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		try {
			String query = "INSERT INTO `SkillTable` (`playeruid`, `skillid`, `experience`) VALUES ('" + playerid + "', '" + skill.name() + "', 0);";
			skilldatabase.execute(query);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * Initialize player skill attributes.
	 *
	 * @param player the player
	 * @param initialize the initialize
	 */
	private void initializePlayerSkillAttributes(Player player, boolean initialize)
	{
		String playerid = player.getUID();

		for (SkillEnum skill : SkillEnum.values())
		{
			if (initialize)
			{
				player.setAttribute(skill.name(), 0);
			}
			else {
				try {
					int experience = getDBAttributeData(playerid, skill);
					player.setAttribute(skill.name(), experience );
					System.out.println(this.getName() + ":  skill " + skill.name() +  ": " + experience);
				}
				catch (Exception e)
				{
					player.setAttribute(skill.name(), 0);
				}
			}
		}
	}

	/** The defaultcraftingskill. */
	private DefaultCraftingSkill defaultcraftingskill;

	/** The logfileparser. */
	private HackManager hackManager;

	/** The sound helper. */
	private SoundHelper soundHelper;

	/**
	 * On player command.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onPlayerCommand(PlayerCommandEvent event)
	{
		//diese Methode wird aufgerufen, wenn das "PlayerCommandEvent" ausgelöst wird
		//d.h. wenn ein Spieler einen Befehl in den Chat eingibt
		String command = event.getCommand();
		System.out.println(this.getName() + ":  onPlayerCommand command: " + command);
		if (command == "/resetskills")
		{
			Player player = event.getPlayer();
			initializePlayerSkillAttributes(player, true);
		}
	}

	/**
	 * On player connect event.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onPlayerConnectEvent(PlayerConnectEvent event)
	{
		Player player = event.getPlayer();

		//If player is a new player (connect for the first time), we insert a new data set
		if(event.isNewPlayer()) {
			// initialize player attributes to zero
			initializePlayerSkillAttributes(player, true);
		}
		else {
			// read skill values from persistent storage
			initializePlayerSkillAttributes(player, false);
		}

		// register key to show skill UI
		player.registerKeys(Key.J, Key.Escape);

		//Important: Set listen for key input to true, otherwise the PlayerKeyEvent will not be triggered
		player.setListenForKeyInput(true);

		SkillUI skillui = new SkillUI(player, this);
		player.setAttribute(SkillUI_Attr_Key, skillui);
	}

	/**
	 * On key input.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onKeyInput(PlayerKeyEvent event)
	{
		System.out.println(this.getName() + ":  onKeyInput");
		//We only want to process key presses (ignore key releases)
		if(event.isPressed())
		{
			if(event.getKey() == Key.J) {
				Player player = event.getPlayer();
				SkillUI skillui = (SkillUI) player.getAttribute(SkillUI_Attr_Key);
				skillui.toggle();
			}
			else if (event.getKey() == Key.Escape)
			{
				Player player = event.getPlayer();
				SkillUI skillui = (SkillUI) player.getAttribute(SkillUI_Attr_Key);
				skillui.hide();
			}
		}
	}

	/**
	 * On player disconnect event.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onPlayerDisconnectEvent(PlayerDisconnectEvent event)
	{
		persistPlayerData(event.getPlayer());
	}

	/**
	 * On player spawn.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onPlayerSpawn(PlayerSpawnEvent event)
	{
		Player player = event.getPlayer();
		ToolDurability toadd = new ToolDurability(player);

		// register event listener
		registerEventListener(toadd);

		player.setAttribute(ToolDurability.attrID, toadd);
	}

	/**
	 * On player game object hit event.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onPlayerGameObjectHitEvent(PlayerGameObjectHitEvent event)
	{
		System.out.println(this.getName() + ":  onPlayerGameObjectHitEvent id: " + event.getGameObject().getID() + " (main)");
	}

	/**
	 * On player hit item event.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onPlayerHitItemEvent(PlayerHitItemEvent event)
	{
		System.out.println(this.getName() + ":  onPlayerHitItemEvent id:" + event.getItem().getGlobalID() + " (main)");
	}

	/**
	 * On player hit object event.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onPlayerHitObjectEvent(PlayerHitObjectEvent event)
	{
		System.out.println(this.getName() + ":  onPlayerHitObjectEvent id:" + event.getGlobalID() + " (main)");
	}

	/**
	 * On player hit terrain event.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onPlayerHitTerrainEvent(PlayerHitTerrainEvent event)
	{
		System.out.println(this.getName() + ":  onPlayerHitTerrainEven (main)");
	}

	/**
	 * On player pickup item event.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onPlayerPickupItemEvent(PlayerPickupItemEvent event)
	{
		System.out.println(this.getName() + ":  onPlayerPickupItemEvent (main)");
	}

	/**
	 * On player craft item event.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onPlayerCraftItemEvent(PlayerCraftItemEvent event)
	{
		System.out.println(this.getName() + ":  onPlayerCraftItemEvent recipe category: " + event.getRecipe().category.name());

		if (defaultcraftingskill != null)
		{
			// if another skill did already catch this craft it's canceled by now
			if ( (! event.isCancelled()) && checkRecipeEvent(event))
			{
				Recipe recipe = event.getRecipe();

				boolean isConstructionRecipe = (recipe.type == RecipeType.Construction);
				boolean isClothingRecipe = (recipe.type == RecipeType.Clothing);
				boolean isBlockRecipe = (recipe.type == RecipeType.Block);

				if (isConstructionRecipe || isClothingRecipe || isBlockRecipe)
				{
					return;
				}

				Player player = event.getPlayer();
				Item item = event.getItem();

				CraftingSpec defaultSpec = new CraftingSpec();
				defaultSpec.minSkill = 0;
				defaultSpec.optimalSkill = 0;
				defaultSpec.craftingTime = 1.0;

				event.setCancelled(true);

				defaultSpec.variant = item.getVariant();

				if (defaultcraftingskill.startCrafting(player, recipe))
				{
					System.out.println(this.getName() + ":  onPlayerCraftItemEvent: " + recipe.name + "|" + item.getName() + " " + item);
					defaultcraftingskill.startCraftingUI(player, this, SkillEnum.RISING_SKILLS_CRAFTING_DEFAULT, recipe, defaultSpec, event.getAmount());
				}
				else {
					String i18nformat = geti18nFormat("NOT_ENOUGH_RESOURCES_TO_CRAFT", player);
					notifyPlayer(player, String.format(i18nformat, recipe.name));
				}
			}
		}
	}

	/**
	 * On vegetation hit.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onPlayerHitVegetation(PlayerHitVegetationEvent event)
	{
		System.out.println(this.getName() + ":  onPlayerHitVegetation id: " + event.getGlobalID() + " (main)");
	}

	/**
	 * On remove vegetation.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onPlayerRemoveVegetation(PlayerRemoveVegetationEvent event)
	{
		System.out.println(this.getName() + ":  onPlayerRemoveVegetation id:" + event.getGlobalID() + " (main)");
	}

	/**
	 * On player remove object.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onPlayerRemoveObject(PlayerRemoveObjectEvent event)
	{
		System.out.println(this.getName() + ":  onPlayerRemoveObject id:" + event.getGlobalID() + " (main)");
	}

	/**
	 * On player remove construction.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onPlayerRemoveConstruction(PlayerRemoveConstructionEvent event)
	{
		System.out.println(this.getName() + ":  onPlayerRemoveConstruction id:" + event.getGlobalID() + " (main)");
	}

	/**
	 * On player change object info event.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onPlayerChangeObjectInfoEvent(PlayerChangeObjectInfoEvent event)
	{
		System.out.println(this.getName() + ":  onPlayerChangeObjectInfoEvent id:" + event.getGlobalID() + " (main)");
	}

	/**
	 * On player change object status event.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onPlayerChangeObjectStatusEvent(PlayerChangeObjectStatusEvent event)
	{
		System.out.println(this.getName() + ":  onPlayerChangeObjectStatusEvent id:" + event.getGlobalID() + " (main)");
	}

	/**
	 * On player inventory to storage event.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onPlayerInventoryToStorageEvent(PlayerInventoryToStorageEvent event)
	{
		System.out.println(this.getName() + ":  onPlayerInventoryToStorageEvent (main)");
	}

	/**
	 * On player storage move item event.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onPlayerStorageMoveItemEvent(PlayerStorageMoveItemEvent event)
	{
		System.out.println(this.getName() + ":  onPlayerStorageMoveItemEvent (main)");
	}

	/**
	 * On player inventory item edit event.
	 *
	 * @param event the event
	 */
	@EventMethod
	public void onPlayerInventoryItemEditEvent(PlayerInventoryItemEditEvent event)
	{
		System.out.println(this.getName() + ":  onPlayerInventoryItemEditEvent (main)");
	}

}
