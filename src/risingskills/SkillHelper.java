/*
 *
 */
package risingskills;

import net.risingworld.api.definitions.Crafting.Recipe;
import net.risingworld.api.definitions.Items;
import net.risingworld.api.definitions.Plants;

// TODO: Auto-generated Javadoc
/**
 * The Class SkillHelper.
 */
public class SkillHelper
{
	/**
	 * Dump all items.
	 */
	static public void dumpAllItems()
	{
		Items.ItemDefinition[] allitems = net.risingworld.api.definitions.Definitions.getAllItemDefinitions();

		for (Items.ItemDefinition item : allitems)
		{
			if (item != null)
			{
				System.out.println("Found: " + item.name + " type: " + item.type.name());
			}
		}
	}

	/**
	 * Dump all trees.
	 */
	static public void dumpAllTrees()
	{
		Plants.PlantDefinition[] allplants = net.risingworld.api.definitions.Definitions.getAllPlantDefinitions();

		for (Plants.PlantDefinition plant : allplants)
		{
			if (plant != null)
			{
				if (plant.type == Plants.Type.Tree)
				{
					System.out.println("Found Tree: " + plant.name + " strength: " + plant.strength + " growthtime: " + plant.growthtime + " size: " + plant.extent.name());
				}
				else if (plant.type == Plants.Type.FruitTree)
				{
					System.out.println("Found FruitTree: " + plant.name + " strength: " + plant.strength + " growthtime: " + plant.growthtime + " size: " + plant.extent.name());
				}
				else if (plant.type == Plants.Type.Trunk)
				{
					System.out.println("Found Trunk: " + plant.name + " strength: " + plant.strength + " growthtime: " + plant.growthtime + " size: " + plant.extent.name());
				}
			}
		}
	}

	/**
	 * Dump all trees.
	 */
	static public void dumpAllStones()
	{
		Plants.PlantDefinition[] allplants = net.risingworld.api.definitions.Definitions.getAllPlantDefinitions();

		for (Plants.PlantDefinition plant : allplants)
		{
			if (plant != null)
			{
				if (plant.type == Plants.Type.Rock)
				{
					System.out.println("Found Rock: " + plant.name + " strength: " + plant.strength + " size: " + plant.extent.name());
				}
			}
		}
	}

	/**
	 * Dump all plants.
	 */
	static public void dumpAllPlants()
	{
		Plants.PlantDefinition[] allplants = net.risingworld.api.definitions.Definitions.getAllPlantDefinitions();

		for (Plants.PlantDefinition plant : allplants)
		{
			if (plant != null)
			{
				if (plant.type == Plants.Type.Plant)
				{
					System.out.println("Found Plant: " + plant.name + " strength: " + plant.strength + " size: " + plant.extent.name() + " gives: " + plant.harvestitem + "/" + plant.destroyitem + "/" + plant.pickupitem);
				}

				if (plant.type == Plants.Type.Crop)
				{
					System.out.println("Found Crop: " + plant.name + " strength: " + plant.strength + " size: " + plant.extent.name() + " gives: " + plant.harvestitem + "/" + plant.destroyitem + "/" + plant.pickupitem);
				}
			}
		}
	}

	/**
	 * Dump all recips.
	 */
	static public void dumpAllRecipes()
	{
		Recipe[] allrecipes = net.risingworld.api.definitions.Definitions.getAllRecipes();

		for (Recipe recipe : allrecipes)
		{
			if (recipe != null)
			{
				System.out.println("Found Recipe: " + recipe.name + " category: " + recipe.category.name() + " sub category:" + recipe.subCategory.name());
			}
		}
	}

	/**
	 * Item exists.
	 *
	 * @param name the name
	 * @return true, if successful
	 */
	static public boolean itemExists(String name)
	{
		return ( net.risingworld.api.definitions.Definitions.getItemDefinition(name) != null);
	}
}
